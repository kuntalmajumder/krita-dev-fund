from unittest import TestCase

import django.test

from looper.money import *

EUR: typing.Callable[[int], Money] = functools.partial(Money, 'EUR')
USD: typing.Callable[[int], Money] = functools.partial(Money, 'USD')


class BasicOperationsTest(TestCase):
    def test_properties(self) -> None:
        self.assertEqual('EUR', EUR(414).currency)
        self.assertEqual(414, EUR(414).cents)

    def test_bad_currency(self) -> None:
        # Unfortunately, to support the MoneyField on new objects, we
        # have to support currency-less Money instances.
        self.assertEqual(55, Money('', 55).cents)

        with self.assertRaises(TypeError):
            Money(44, 55)  # type: ignore
        with self.assertRaises(TypeError):
            Money(None, 55)  # type: ignore

    def test_bad_cents(self) -> None:
        with self.assertRaises(TypeError):
            Money('EUR', 44.3)  # type: ignore
        with self.assertRaises(TypeError):
            Money('EUR', 44j)  # type: ignore

    def test_str(self) -> None:
        self.assertEqual('EUR\u00A05.00', str(EUR(500)))
        self.assertEqual('USD\u00A00.01', str(Money('USD', 1)))

    def test_repr(self) -> None:
        self.assertEqual("Money(currency='EUR', cents=12344)", repr(EUR(12344)))

    def test_arithmetic_happy(self) -> None:
        self.assertEqual(EUR(-4315), +EUR(-4315))
        self.assertEqual(EUR(-4315), -EUR(4315))
        self.assertEqual(EUR(4315), EUR(315) + EUR(4000))
        self.assertEqual(EUR(-3685), EUR(315) - EUR(4000))
        self.assertEqual(EUR(15), 3 * EUR(5))
        self.assertEqual(EUR(15), EUR(5) * 3)

    def test_ordering(self) -> None:
        self.assertLess(EUR(15), EUR(16))
        self.assertLessEqual(EUR(15), EUR(16))
        self.assertLessEqual(EUR(15), EUR(15))
        self.assertGreater(EUR(16), EUR(15))
        self.assertGreaterEqual(EUR(16), EUR(15))
        self.assertGreaterEqual(EUR(15), EUR(15))
        self.assertGreaterEqual(EUR(15), -EUR(100))

        with self.assertRaises(TypeError):
            EUR(15) < 30

    def test_arithmetic_unsupported(self):
        m = EUR(56123)
        with self.assertRaises(TypeError):
            m + 3
        with self.assertRaises(TypeError):
            3 + m
        with self.assertRaises(TypeError):
            m - 3
        with self.assertRaises(TypeError):
            3 - m
        with self.assertRaises(TypeError):
            m * m
        with self.assertRaises(TypeError):
            3.0 * m
        with self.assertRaises(TypeError):
            m * 3.0

    def test_immutability(self) -> None:
        m1 = EUR(4123)
        m2 = m1
        m1 += EUR(1)
        self.assertEqual(EUR(4124), m1)
        self.assertEqual(EUR(4123), m2)

    def test_true_division(self) -> None:
        self.assertEqual([EUR(4), EUR(4), EUR(3), EUR(3)], EUR(14) / 4)

        with self.assertRaises(TypeError):
            EUR(14) / 4.0  # type: ignore
        with self.assertRaises(ValueError):
            EUR(14) / -4
        with self.assertRaises(ZeroDivisionError):
            EUR(14) / 0

    def test_floor_division(self) -> None:
        self.assertEqual(EUR(3), EUR(14) // 4)

        self.assertEqual(EUR(3), EUR(14) // 4.0)
        self.assertEqual(EUR(3), EUR(14) // 4.6)
        self.assertEqual(EUR(2), EUR(14) // 4.7)

        with self.assertRaises(TypeError):
            EUR(14) // 4.0j  # type: ignore
        with self.assertRaises(TypeError):
            EUR(14) // EUR(4)  # type: ignore
        with self.assertRaises(ValueError):
            EUR(14) // -4
        with self.assertRaises(ZeroDivisionError):
            EUR(14) // 0

    def test_ratio(self):
        self.assertEqual(0.0, EUR(0) / EUR(4))
        self.assertEqual(1.0, EUR(4) / EUR(4))
        self.assertEqual(-2.0, EUR(4) / EUR(-2))
        self.assertAlmostEqual(1.15, USD(17332) / USD(15072), places=3)

        with self.assertRaises(CurrencyMismatch):
            EUR(14) / USD(14)

    def test_equality(self) -> None:
        self.assertEqual(hash(('EUR', 513)), hash(EUR(513)))
        m1 = EUR(551)
        m2 = EUR(551)
        self.assertIsNot(m1, m2)
        self.assertEqual(m1, m2)
        self.assertLess(m1, EUR(1000))
        self.assertNotEqual(m1, 'EUR\u00A0551')

    def test_bool(self) -> None:
        self.assertTrue(EUR(1))
        self.assertTrue(EUR(-1))
        self.assertFalse(EUR(0))

    def test_currency_mismatch(self) -> None:
        with self.assertRaises(CurrencyMismatch):
            EUR(14) + USD(44)
        with self.assertRaises(CurrencyMismatch):
            EUR(14) - USD(44)

        with self.assertRaises(CurrencyMismatch):
            EUR(14) < USD(14)

    def test_as_decimal_string(self) -> None:
        m1 = EUR(1000)
        self.assertEqual(m1.decimals_string, '10.00')
        m2 = EUR(9999)
        self.assertEqual(m2.decimals_string, '99.99')
        m2 = EUR(1010)
        self.assertEqual(m2.decimals_string, '10.10')
        m2 = EUR(1009)
        self.assertEqual(m2.decimals_string, '10.09')
        m2 = EUR(1033)
        self.assertEqual(m2.decimals_string, '10.33')

    def test_with_currency_symbol(self) -> None:
        self.assertEqual('€\u00A010.00', EUR(1000).with_currency_symbol())
        self.assertEqual('$\u00A010.00', USD(1000).with_currency_symbol())
        self.assertEqual('¥\u00A010.00', Money('JPY', 1000).with_currency_symbol())

    def test_with_currency_symbol_nonocents(self) -> None:
        self.assertEqual('€\u00A010', EUR(1000).with_currency_symbol_nonocents())
        self.assertEqual('$\u00A010.50', USD(1050).with_currency_symbol_nonocents())
        self.assertEqual('¥\u00A00.99', Money('JPY', 99).with_currency_symbol_nonocents())

    def test_with_currency_symbol_rounded(self) -> None:
        self.assertEqual('€\u00A010', EUR(1000).with_currency_symbol_rounded())
        self.assertEqual('$\u00A010', USD(1050).with_currency_symbol_rounded())
        self.assertEqual('$\u00A011', USD(1051).with_currency_symbol_rounded())
        self.assertEqual('$\u00A011', USD(1149).with_currency_symbol_rounded())
        self.assertEqual('$\u00A012', USD(1150).with_currency_symbol_rounded())
        self.assertEqual('¥\u00A01', Money('JPY', 99).with_currency_symbol_rounded())


class SumTest(TestCase):
    def test_empty(self) -> None:
        self.assertEqual({}, sum_per_currency([]))

    def test_one_currency_one_amount(self) -> None:
        self.assertEqual({'HRK': Money('HRK', 47)}, sum_per_currency([Money('HRK', 47)]))

    def test_one_currency_multiple_amounts(self) -> None:
        self.assertEqual({'HRK': Money('HRK', 374)},
                         sum_per_currency([Money('HRK', 47), Money('HRK', 327)]))

    def test_multiple_currencies(self) -> None:
        self.assertEqual(
            {'HRK': Money('HRK', 47 + 327),
             'SGD': Money('SGD', 413 + 385)},
            sum_per_currency([
                Money('HRK', 47),
                Money('HRK', 327),
                Money('SGD', 413),
                Money('SGD', 385),
            ])
        )

        self.assertEqual(
            {'HRK': Money('HRK', 47 + 327),
             'SGD': Money('SGD', 413 + 385),
             'USD': Money('USD', 161)},
            sum_per_currency([
                Money('HRK', 327),
                Money('SGD', 413),
                Money('HRK', 47),
                Money('USD', 161),
                Money('SGD', 385),
            ])
        )


@django.test.override_settings(
    LOOPER_CONVERTION_RATES_FROM_EURO={
        'EUR': 1.0,
        'USD': 1.15,  # US$ 1.15 = € 1.00
    }
)
class SumToEuroTest(django.test.TestCase):
    def test_empty(self) -> None:
        self.assertEqual(Money('EUR', 0), sum_to_euros([]))

    def test_single_euro(self) -> None:
        self.assertEqual(Money('EUR', 215), sum_to_euros([Money('EUR', 215)]))

    def test_multiple_euro(self) -> None:
        self.assertEqual(Money('EUR', 315), sum_to_euros([Money('EUR', 215), Money('EUR', 100)]))

    def test_multiple_currencies(self) -> None:
        self.assertEqual(Money('EUR', 287), sum_to_euros([Money('EUR', 200), Money('USD', 100)]))


@django.test.override_settings(
    LOOPER_CONVERTION_RATES_FROM_EURO={
        'EUR': 1.0,
        'USD': 1.15,  # US$ 1.15 = € 1.00
        'SGD': 1.59,  # SG$ 1.59 = € 1.00
    }
)
class ConvertCurrencyTest(django.test.TestCase):
    def test_identical(self) -> None:
        amt = Money('EUR', 47)
        self.assertIs(amt, convert_currency(amt, to_currency='EUR'))

    def test_usd_to_eur(self) -> None:
        self.assertEqual(Money('EUR', 435), convert_currency(Money('USD', 500), to_currency='EUR'))

    def test_eur_to_usd(self) -> None:
        self.assertEqual(Money('USD', 500), convert_currency(Money('EUR', 435), to_currency='USD'))

    def test_sgd_to_usd(self) -> None:
        self.assertEqual(Money('USD', 34230),
                         convert_currency(Money('SGD', 47327), to_currency='USD'))


class PickleMoneyTest(TestCase):
    def test_pickle(self) -> None:
        import pickle

        amt = Money('HRK', 716)
        pickled = pickle.dumps(amt)
        unpickled = pickle.loads(pickled)

        self.assertEqual(amt, unpickled)
