Dear {{ customer.full_name|default:email }},

Automatic payment of your Blender Development Fund membership failed. We have tried
{{ order.collection_attempts }} times, but none of those attempts was succesful. As a result, we have suspended
your membership for now.

The error we received from the payment provider was: {{ failure_message }}.

To resume your membership, please visit {{ pay_url }}]


Kind Regards,

Blender Foundation
