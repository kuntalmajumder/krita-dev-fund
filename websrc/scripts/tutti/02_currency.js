function set_preferred_currency(currency) {
    $.post('/preferred-currency', {'preferred_currency': currency})
    .fail(function(error) {
        console.log('Error setting preferred currency:', error);
    })
    .done(function() {
        // Show the change in currency by reloading the current window.
        window.location.reload();
    })
}
