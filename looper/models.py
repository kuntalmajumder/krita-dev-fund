import datetime
import functools
import logging
import typing

import dateutil.relativedelta
from django.contrib.auth.models import User
from django.db import models
import django.utils.timezone
from django.urls import reverse
from django.utils.html import format_html
from django_countries.fields import CountryField

from looper.money import Money, CurrencyMismatch
from . import gateways, model_mixins, admin_log, form_fields
import looper.signals
import looper.exceptions

DEFAULT_CURRENCY = 'EUR'
CURRENCIES = (
    ('USD', 'USD'),
    ('EUR', 'EUR'),
)

log = logging.getLogger(__name__)
UpdateFieldsType = typing.Optional[typing.Set[str]]
"""The type used by Django's save(update_field=...) parameter."""


def requires_status(*required_statuses):
    """Function decorator to assert a model has a certain status.

    :raise looper.exceptions.IncorrectStatusError:
    """
    _required_statuses = set(required_statuses)

    def decorator(wrapped):
        @functools.wraps(wrapped)
        def checker(self, *args, **kwargs):
            if self.status not in _required_statuses:
                raise looper.exceptions.IncorrectStatusError(
                    f'To call {self.__class__}.{wrapped}, {self} needs to have '
                    f'status in {_required_statuses} but it has {self.status!r}',
                    self.status, _required_statuses)
            return wrapped(self, *args, **kwargs)

        return checker

    return decorator


class CurrencyField(models.CharField):
    def __init__(self, **kwargs):
        kwargs.setdefault('max_length', 3)
        kwargs.setdefault('choices', CURRENCIES)
        kwargs.setdefault('default', DEFAULT_CURRENCY)
        super().__init__(**kwargs)


class MoneyFieldDescriptor:
    """Ensures that a MoneyField produces/accepts a Money instance.

    A MoneyField accepts either an int (number of cents) or a Money instance.
    In the former case the currency is taken from the 'currency_field'
    attribute of the model instance. In the latter case that same attribute
    is used to ensure consistent currencies.
    """

    def __init__(self, field):
        self.field = field

    def __get__(self, instance, cls=None) -> typing.Optional[Money]:
        if instance is None:
            return None
        try:
            return instance.__dict__[self.field.name]
        except KeyError as ex:
            raise AttributeError(f'No such attribute: {ex!s}')

    def __set__(self, instance, value: typing.Union[None, int, Money]):
        try:
            instance_currency = instance.__dict__[self.field.currency_field]
        except KeyError:
            raise AttributeError(f'{instance!r} has no property {self.field.currency_field}')

        if value is None:
            value_to_set = None
        elif isinstance(value, int):
            value_to_set = Money(instance_currency, value)
        elif isinstance(value, Money):
            if value.currency != instance_currency:
                # This message has to be carefully constructed, as __set__()
                # can be called during object construction. As such, properties
                # accessed in __str__() may not exist or can cause unintended
                # database lookups.
                raise CurrencyMismatch(f'Currency is {instance_currency!r} but trying to set '
                                       f'.{self.field.name} = {value!r}')
            value_to_set = value
        else:
            raise TypeError(f'Unsupported type {type(value)}')
        instance.__dict__[self.field.name] = value_to_set


class MoneyField(models.IntegerField):
    """Monetary Amount database field.

    The amount is stored in cents (that is, 1/100th of currency units).

    To use this field, the field referenced by the 'currency_field'
    parameter must exist already. That is, it must be declared on the
    model class before the MoneyField() instance that references it.

    To keep mypy happy, add a type declaration 'fieldname: Money' when
    using this model field.
    """

    def __init__(self, *args, currency_field='currency', **kwargs):
        self.currency_field = currency_field
        super().__init__(*args, **kwargs)

    def deconstruct(self):
        """Return parameters for __init__ to recreate this field."""
        name, path, args, kwargs = super().deconstruct()
        kwargs['currency_field'] = self.currency_field
        return name, path, args, kwargs

    def get_prep_value(self, value: typing.Union[None, int, Money]) -> typing.Optional[int]:
        """Return field's value prepared for saving into a database."""
        if value is None:
            return None
        if isinstance(value, int):
            # The default value for an amount can be given as an integer.
            return value
        assert isinstance(value, Money)
        return value.cents

    def contribute_to_class(self, cls, name, **kwargs):
        """Ensure our descriptor is called when getting/setting the attribute."""
        super().contribute_to_class(cls, name, **kwargs)
        setattr(cls, self.name, MoneyFieldDescriptor(self))

    def formfield(self, **kwargs):
        """Default to MoneyFormField if form_class is not specified."""
        return super().formfield(**{
            'form_class': form_fields.MoneyFormField,
            **kwargs,
            'widget': form_fields.MoneyInput,
        })

    def to_python(self, value: typing.Union[None, int, str, Money]) -> typing.Optional[int]:
        """The result of this function feeds the MoneyFieldDescriptor.

        I think.

        Note that this function is also used when deserialising values from
        JSON, in which case the Money value was serialised to the string
        '{currency}\u00A0{amount}'.
        """
        if isinstance(value, str):
            return Money.from_str(value).cents
        if isinstance(value, Money):
            return value.cents
        return super().to_python(value)

    def get_attname_column(self):
        """Add '_in_cents' to the database column name."""
        attname, column = super().get_attname_column()
        return attname, f'{column}_in_cents'


class Customer(models.Model):
    """Extended properties for the existing user.

    It references:
    - addresses
    - payment_methods

    It could also reference:
    - user token for each Gateway it uses
    """
    _log = log.getChild('Customer')

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='customer')
    full_name = models.CharField(max_length=255, blank=True, default='')
    company = models.CharField(max_length=255, blank=True, default='')
    billing_email = models.EmailField(max_length=255, help_text='For billing notifications.')
    vat_number = models.CharField(max_length=255, blank=True, default='')  # Get package
    tax_exempt = models.BooleanField(default=False)

    @property
    def billing_address(self) -> 'Address':
        try:
            address = self.user.address_set.get(category='billing')
        except Address.DoesNotExist:
            # TODO Is it a good idea to return a new Address or should we return None?
            address = Address(user=self.user)
        return address
        # TODO Ensure one 'billing' address per user

    def billing_address_as_text(self) -> str:
        from django_countries.fields import Country

        def to_str(element):
            if isinstance(element, Country):
                return element.name
            return str(element)

        return '\n'.join(to_str(elem) for elem in self.billing_address.public_values if elem)

    @property
    def payment_method_default(self) -> typing.Optional['PaymentMethod']:
        return self.user.paymentmethod_set.filter(is_deleted=False).first()

    def gateway_customer_id(self, gateway: 'Gateway') -> str:
        """Get the customer ID on the given gateway.

        :return: the ID string, or '' when the customer has no customer ID on
            this gateway.
        """
        try:
            gateway_customer = self.user.gatewaycustomerid_set.get(gateway=gateway)
        except GatewayCustomerId.DoesNotExist:
            return ''
        return gateway_customer.gateway_customer_id

    def gateway_customer_id_get_or_create(self, gateway: 'Gateway') -> str:
        """Get or create a customer on the gateway.

        :param gateway:
        :return: The customer's ID at that particular gateway.
        :raise: looper.exceptions.GatewayError if the customer could not be created.
        """
        try:
            gateway_customer = self.user.gatewaycustomerid_set.get(gateway=gateway)
        except GatewayCustomerId.DoesNotExist:
            gateway_customer_id = gateway.provider.customer_create()
            gateway_customer = self.user.gatewaycustomerid_set.create(
                gateway_customer_id=gateway_customer_id, gateway=gateway)
        return gateway_customer.gateway_customer_id

    def payment_method_add(self,
                           payment_method_nonce: str,
                           gateway: 'Gateway') -> 'PaymentMethod':
        """Add a payment method for the customer on the gateway.

        :raise: looper.exceptions.GatewayError if the payment method could not be added.
        """
        # Check if the customer already exists on the provided gateway.
        gateway_customer_id = self.gateway_customer_id_get_or_create(gateway)

        pm_info = gateway.provider.payment_method_create(
            payment_method_nonce, gateway_customer_id)

        # I'd rather just query and handle duplicates in the django.db.IntegrityError exception
        # handler, but Django doesn't allow me to do queries there (raises a
        # TransactionManagementError).
        paymeth: PaymentMethod = self.user.paymentmethod_set.filter(
            gateway=gateway, token=pm_info.token).first()
        if not paymeth:
            self._log.debug('Storing new payment method for customer pk=%d', self.pk)
            method_type = pm_info.type_for_database()
            assert isinstance(method_type, str), f'expected str, not {method_type!r}'
            recog_name = pm_info.recognisable_name()
            return self.user.paymentmethod_set.create(gateway=gateway,
                                                      token=pm_info.token,
                                                      method_type=method_type,
                                                      recognisable_name=recog_name)

        if paymeth.is_deleted:
            # We're re-adding a previously-deleted payment method, so just restore it.
            paymeth.undelete()

        self._log.debug('Updating existing payment method for customer pk=%d', self.pk)
        paymeth.recognisable_name = pm_info.recognisable_name()
        paymeth.method_type = pm_info.type_for_database()
        paymeth.save(update_fields={'recognisable_name', 'method_type'})
        return paymeth

    def __str__(self):
        if not self.full_name:
            return f'Customer {self.pk}'
        return self.full_name


class Gateway(models.Model):
    """Payment gateway data.

    Links Customers to a specific payment gateway.
    """

    GATEWAY_CHOICES = [(name, name.title()) for name in gateways.Registry.gateway_names()]
    name = models.CharField(max_length=32, unique=True, choices=GATEWAY_CHOICES)
    users = models.ManyToManyField(User, through='GatewayCustomerId')
    is_default = models.BooleanField(default=False)
    frontend_name = models.CharField(
        max_length=64, help_text='User-facing name for the gateway in payment forms.')
    form_description = models.TextField(
        blank=True,
        help_text='Shown in payment forms when this gateway is selected. This is interpreted as '
                  'HTML, so be careful and HTML-escape what needs to be HTML-escaped.')
    log = log.getChild('Gateway')

    @classmethod
    def default(cls):
        return cls.objects.get(is_default=True)

    @property
    def provider(self) -> gateways.AbstractPaymentGateway:
        return gateways.Registry.instance_for(self.name)

    def save(self, *args, **kwargs):
        """Ensure that only one Gateway is the default one."""
        if self.is_default:
            for old_default in Gateway.objects.filter(is_default=True):
                self.log.info('Switching default gateway from %r to %r', old_default, self)
                old_default.is_default = False
                old_default.save(update_fields={'is_default'})
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class GatewayCustomerId(models.Model):
    """Identity of the Customer on the payment Gatweay.

    Stores a reference to a customer, to fetch payment methods on the Gateway.
    We could do without this table, specifying to the Gateway our Customer id,
    but that feels too implicit and there is no guarantee that all gateways support this.
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    gateway = models.ForeignKey(Gateway, on_delete=models.CASCADE)
    gateway_customer_id = models.CharField(max_length=255)


class PaymentMethod(model_mixins.CreatedUpdatedMixin, models.Model):
    """Called Source in Stripe.

    Optionally create CreditCard, BankAccount, etc.
    """

    class Meta:
        unique_together = ('gateway', 'token', 'user')

    DEFAULT_METHOD_TYPE = 'cc'
    METHOD_TYPES = (
        ('cc', 'Credit Card'),
        ('pp', 'PayPal'),
        ('ba', 'Bank'),
    )
    gateway = models.ForeignKey(Gateway, on_delete=models.CASCADE)
    method_type = models.CharField(choices=METHOD_TYPES, blank=True, max_length=255)
    token = models.CharField(max_length=255)  # The important information
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_deleted = models.BooleanField(default=False)
    recognisable_name = models.CharField(max_length=255, default='', blank=True)

    def gateway_properties(self) -> typing.Optional[looper.gateways.PaymentMethodInfo]:
        """Retrieve from the gateway further information.

        Depending on the method type, we might get:
        - the last 4 digits of a credit card
        - an image url of the card type/PayPal

        Returns None if the token does not correspond to a Payment Method
        on the payment gateway.
        """
        log.debug('Fetching gateway properties of payment method pk=%d', self.pk)
        try:
            pm_info = self.gateway.provider.find_customer_payment_method(self.token)
        except looper.exceptions.NotFoundError:
            log.info('Tried to find payment method that does not exist')
            return None

        # Now that we have fresh info from the payment gateway, update our internal bookkeeping.
        recog_name = pm_info.recognisable_name()
        if recog_name != self.recognisable_name:
            log.debug('Updating recognisable name of payment method pk=%d', self.pk)
            self.recognisable_name = recog_name
            self.save(update_fields={'recognisable_name'})

        type_for_db = pm_info.type_for_database()
        if type_for_db != self.method_type:
            log.debug('Updating method_type of payment method pk=%d', self.pk)
            self.method_type = type_for_db
            self.save(update_fields={'method_type'})

        return pm_info

    def delete(self, *args, using=None, **kwargs):
        """Soft-delete instead of really deleting.

        This allows us to keep references to this payment method.
        Note that deleting does really delete the payment method at the
        payment gateway; the record we keep is for our own history only.
        """

        # TODO check if the payment method is connected to a pending subscription or an order
        # TODO(Sybren): handle exceptions communicating with the gateway.
        self.gateway.provider.payment_method_delete(self.token)

        log.debug('Soft-deleting payment method pk=%d', self.pk)
        self.is_deleted = True
        self.save(using=using, update_fields={'is_deleted'})

    def undelete(self):
        """Un-soft-deletes the instance.

        Note that this assumes the user has re-added the same payment method
        at the payment gateway, and that this resulted in the same payment
        token. Otherwise it should just be a new PaymentMethod instance.
        """
        log.debug('Un-deleting payment method pk=%d', self.pk)
        self.is_deleted = False
        self.save(update_fields={'is_deleted'})

    def __str__(self):
        as_str = f'{self.gateway.name} – {self.recognisable_name or self.method_type}'
        if self.is_deleted:
            return f'{as_str} (deleted)'
        return as_str


class Address(models.Model):
    # These are the fields used to create forms, and publicly displayed
    PUBLIC_FIELDS = ['full_name', 'company', 'street_address', 'extended_address', 'locality',
                     'postal_code', 'region', 'country']
    DEFAULT_ADDRESS_CATEGORY = 'billing'
    ADDRESS_CATEGORIES = (
        ('billing', 'Billing'),
        ('shipping', 'Shipping'),
    )

    category = models.CharField(choices=ADDRESS_CATEGORIES,
                                default=DEFAULT_ADDRESS_CATEGORY, max_length=20)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=255)
    company = models.CharField(max_length=255, default='', blank=True)
    street_address = models.CharField(max_length=255, default='', blank=True)
    extended_address = models.CharField(max_length=255, default='', blank=True)
    locality = models.CharField(max_length=255, default='', blank=True,
                                verbose_name='City')
    postal_code = models.CharField(max_length=255,
                                   verbose_name='ZIP/Postal Code',
                                   blank=True, default='')
    region = models.CharField(max_length=255,
                              verbose_name='State/Province/Region',
                              blank=True, default='')
    country = CountryField(blank=True)

    @property
    def public_values(self) -> typing.Iterable[str]:
        return (getattr(self, f) for f in self.PUBLIC_FIELDS)

    @property
    def as_dict(self):
        return {f: getattr(self, f) for f in self.PUBLIC_FIELDS}

    def __str__(self) -> str:
        return f'{self.category} address of {self.user}'.capitalize()


class Product(model_mixins.CreatedUpdatedMixin, models.Model):
    """Can be a good or a service, like 'Development Fund'."""
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Plan(models.Model):
    """Acts as a variation of the product, like 'Gold' for 'Development Fund'."""
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)  # User-facing name
    description = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def variation_for_currency(self, currency: str) -> typing.Optional['PlanVariation']:
        """Determine the default variation for the given currency.

        Returns None when there is no plan variation for the currency at all.
        """

        default_variation = self.variations.active() \
            .filter(currency=currency) \
            .order_by('-is_default_for_currency', '-price') \
            .first()
        return default_variation


class PlanVariationManager(models.Manager):

    def active(self) -> models.QuerySet:
        """Return QuerySet of only the active plan variations."""
        return self.filter(is_active=True)


class PlanVariation(models.Model):
    DEFAULT_INTERVAL_UNIT = 'month'
    INTERVAL_UNITS = (
        ('day', 'Day'),
        ('week', 'Week'),
        ('month', 'Month'),
        ('year', 'Year'),
    )
    DEFAULT_COLLECTION_METHOD = 'automatic'
    COLLECTION_METHODS = (
        ('automatic', 'Automatic'),
        ('manual', 'Manual'),
    )
    plan = models.ForeignKey(Plan, on_delete=models.CASCADE,
                             related_name='variations')
    currency = CurrencyField()
    price: Money = MoneyField(help_text='Including tax.')
    interval_unit = models.CharField(choices=INTERVAL_UNITS,
                                     default=DEFAULT_INTERVAL_UNIT, max_length=50)
    interval_length = models.PositiveIntegerField(default=1)
    collection_method = models.CharField(choices=COLLECTION_METHODS,
                                         default=DEFAULT_COLLECTION_METHOD, max_length=20)
    is_active = models.BooleanField(default=True)
    is_default_for_currency = models.BooleanField(default=False)

    objects = PlanVariationManager()

    class Meta:
        unique_together = [
            ('plan', 'currency', 'interval_unit', 'interval_length', 'collection_method')
        ]
        ordering = ('-is_active', 'currency', '-price', 'collection_method')

    @property
    def price_per_month(self) -> Money:
        """Returns the rounded price per month for this plan variation.

        Note that this is a lossy operation, as it rounds down to entire cents.
        """
        months = {
            'day': 12 / 365,  # Approximate average nr of months in a day.
            'week': 12 / 52,  # Approximate average nr of months in a week.
            'month': 1,
            'year': 12,
        }
        months_per_interval = months[self.interval_unit] * self.interval_length
        return self.price // months_per_interval

    def __str__(self):
        return f"{self.plan.name} - {self.currency} - {self.interval_length} {self.interval_unit}"

    def __repr__(self) -> str:
        return f'PlanVariation(pk={self.pk}, plan.name={self.plan.name!r}, price={self.price!r}, ' \
               f'interval_unit={self.interval_unit!r}, interval_length={self.interval_length}, ' \
               f'collection_method={self.collection_method})'

    def save(self, *args, **kwargs):
        if self.is_default_for_currency:
            # Make sure there is only one default.
            self.__class__.objects \
                .filter(plan=self.plan, currency=self.currency) \
                .exclude(pk=self.pk) \
                .update(is_default_for_currency=False)

        super().save(*args, **kwargs)


class SharedFields(models.Model):
    """Fields shared between Subscription and Order."""

    class Meta:  # type: ignore
        abstract = True

    DEFAULT_COLLECTION_METHOD = 'automatic'

    # If you change COLLECTION_METHODS, also update looper.clock.AbstractClock.renew_subscription.
    COLLECTION_METHODS = (
        ('automatic', 'Automatic'),
        ('manual', 'Manual (by subscriber)'),
        ('managed', 'Managed (manual by staff)'),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    payment_method = models.ForeignKey(PaymentMethod, null=True, blank=True,
                                       on_delete=models.CASCADE)

    currency = CurrencyField()
    price: Money = MoneyField(default=0, help_text='Including tax.')
    collection_method = models.CharField(choices=COLLECTION_METHODS,
                                         default=DEFAULT_COLLECTION_METHOD, max_length=20)

    tax: Money = MoneyField(blank=True, default=0)
    tax_type = models.CharField(max_length=20, blank=True, default='')
    tax_region = CountryField(blank=True, default='')

    @classmethod
    def field_names(cls) -> typing.Iterable[str]:
        return (f.name for f in cls._meta.fields)


class SubscriptionManager(models.Manager):

    def payable(self) -> models.QuerySet:
        """Return subscriptions that can still be paid for."""
        return self.filter(status__in={'active', 'on-hold'})


# Ignoring type because of the inner Meta class of the two mix-ins.
class Subscription(model_mixins.RecordModifcationMixin,  # type: ignore
                   model_mixins.CreatedUpdatedMixin,
                   model_mixins.StateMachineMixin,
                   SharedFields,
                   models.Model):
    """Entity the defines orders and billing."""
    record_modification_fields = {'status', 'is_active', 'user_id'}
    log = log.getChild('Subscription')

    DEFAULT_INTERVAL_UNIT = 'month'
    INTERVAL_UNITS = (
        ('day', 'Day'),
        ('week', 'Week'),
        ('month', 'Month'),
        ('year', 'Year'),
    )

    # How many of these fit in a month.
    _UNITS_PER_MONTH = {
        'day': 30,
        'week': 30 / 7,
        'month': 1,
        'year': 1 / 12,
    }

    DEFAULT_STATUS = 'on-hold'
    STATUSES = (
        ('active', 'Active'),
        ('on-hold', 'On Hold'),
        ('pending-cancellation', 'Pending Cancellation'),
        ('cancelled', 'Cancelled'),
    )
    _ACTIVE_STATUSES = {'active', 'pending-cancellation'}
    """Use subscription.is_active to test for the active status."""

    # Map 'current status': {set of allowed new statuses}
    VALID_STATUS_TRANSITIONS: typing.Mapping[str, typing.Set[str]] = {
        'active': {'on-hold', 'pending-cancellation', 'cancelled'},
        'on-hold': {'active', 'cancelled'},
        'pending-cancellation': {'active', 'cancelled'},
        'cancelled': set(),
    }

    plan = models.ForeignKey(Plan, on_delete=models.CASCADE)
    status = models.CharField(
        choices=STATUSES, default=DEFAULT_STATUS, max_length=20,
        help_text="Note that setting the status to 'Active' will also set the "
                  "next payment date one renewal interval from now.")

    interval_unit = models.CharField(
        choices=INTERVAL_UNITS,
        default=DEFAULT_INTERVAL_UNIT, max_length=50,
        help_text='The units in which "interval length" is expressed')
    interval_length = models.IntegerField(
        default=1, help_text='How many "interval units" is each billing cycle.')
    intervals_elapsed = models.IntegerField(
        default=0, null=False, blank=True,
        help_text='How many billing cycles have happened.')

    started_at = models.DateTimeField(null=True, blank=True, help_text='Date of first activation.')
    cancelled_at = models.DateTimeField(null=True, blank=True)

    current_interval_started_at = models.DateTimeField(null=True, blank=True)
    next_payment = models.DateTimeField(
        null=True, blank=True, help_text='When the next payment is due.')

    last_notification = models.DateTimeField(
        null=True, blank=True,
        help_text='When Ton was last sent a notification that this subscription passed its next '
                  'payment date. Only used for managed subscriptions.')

    objects = SubscriptionManager()

    @property
    def monthly_rounded_price(self) -> Money:
        """Compute the price per month, rounded to entire cents.

        Note that using montly_rounded_price() * 12 to get the
        yearly price is wrong, due to rounding errors. If you want
        to know that, just implement yearly_price() or something.
        """
        units_per_month = self._UNITS_PER_MONTH[self.interval_unit]
        price_in_cents = self.price.cents * units_per_month / self.interval_length
        return Money(currency=self.currency, cents=int(round(price_in_cents)))

    @property
    def interval(self) -> dateutil.relativedelta.relativedelta:
        """Return the renewal interval as relativedelta.

        The relativedelta can be added to regular `datetime.datetime` objects.
        """
        kwargs = {f"{self.interval_unit}s": self.interval_length}
        return dateutil.relativedelta.relativedelta(**kwargs)

    @property
    def can_be_activated(self) -> bool:
        """Whether a status transition to 'active' is allowed."""
        return self.may_transition_to('active')

    @property
    def is_active(self) -> bool:
        """Whether the subscription is actively usable.

        This is of course true in status 'active', but also in status
        'pending-cancellation'.
        """
        return self.status in self._ACTIVE_STATUSES

    @property
    def next_payment_in_future(self) -> bool:
        """True iff the next_payment is known and in the future."""
        if self.next_payment is None:
            return False
        now = django.utils.timezone.now()
        return self.next_payment > now

    @property
    def next_payment_in_past(self) -> bool:
        """True iff the next_payment is known and in the past."""
        if self.next_payment is None:
            return False
        now = django.utils.timezone.now()
        return self.next_payment < now

    @property
    def notification_required(self) -> bool:
        """True iff next_payment is in the past and the last notification was from even earlier."""

        if self.next_payment_in_future:
            return False

        return self.last_notification is None or self.last_notification < self.next_payment

    @requires_status('on-hold', 'active')
    def cancel_subscription(self) -> None:
        """Either directly cancels the subscription or moves to pending-cancellation."""

        old_status = self.status
        if self.status == 'on-hold':
            self.status = 'cancelled'
        elif self.status == 'active':
            self.status = 'pending-cancellation'
        else:
            self.log.warning('Not cancelling subscription %d because '
                             'it is in uncancellable status %r',
                             self.pk, self.status)
            return
        self.log.info('Cancelling subscription %d from status %r to %r',
                      self.pk, old_status, self.status)
        self.save(update_fields={'status'})

    def latest_order(self) -> typing.Optional['Order']:
        """Returns the last order for this Subscription.

        Since an Order is usually automatically created when a subscription
        is created, this mostly return an Order. The only exception is those
        subscriptions for which collection_method='managed'; those never have
        any orders attached.
        """
        try:
            return self.order_set.latest()
        except Order.DoesNotExist:
            return None

    def save(self, *args, update_fields: UpdateFieldsType = None, **kwargs) -> None:
        """Act on state changes."""
        is_new = not self.pk
        was_changed, old_state = self.pre_save_record()

        if was_changed:
            self._handle_status_change_pre_save(old_state, update_fields)

        super().save(*args, update_fields=update_fields, **kwargs)

        if old_state.get('user_id') != self.user_id:
            self._handle_user_changed(old_state.get('user_id'))

        if was_changed:
            self._handle_status_change_post_save(old_state)

    def _handle_status_change_pre_save(self, old_state: typing.Mapping[str, typing.Any],
                                       update_fields: UpdateFieldsType) -> None:
        """Handle status changes that should be reflected in the to-be-saved instance.

        :param old_state: The current state in the database (which is about to be overwritten).
        :param update_fields: The `update_fields` param passed to the save() method. If you
            change fields in `self` that should be persisted, be sure to update this in-place.
        """
        old_status = old_state.get('status', '')
        if old_status == self.status:
            return

        was_active = old_state.get('is_active', False)
        if self.is_active and not was_active:
            self._on_activation_pre_save(old_status, update_fields)
        elif not self.is_active and was_active:
            self._on_deactivation_pre_save(old_status, update_fields)

        if self.status == 'cancelled':
            self._on_cancelled(update_fields)

    def _handle_user_changed(self, old_user_id: typing.Optional[int]) -> None:
        """Reset the payment method."""
        if old_user_id is None:
            return

        new_user_id = self.user_id
        self.log.info('Subscription %d changed user from %d to %s',
                      self.pk, old_user_id, new_user_id)

        for order in self.order_set.all():
            self.log.debug('transferring order %d from user %d to %d',
                           order.id, order.user_id, new_user_id)
            # Setting the payment method to None and transferring the transactions
            # to the new user too could be done in the Order.save() method, but I
            # don't want to copy the same logic there too. Only subscriptions should
            # change ownership directly; the rest should just follow.
            order.user_id = new_user_id
            order.payment_method_id = None
            order.save(update_fields={'user_id', 'payment_method_id'})
            for transaction in order.transaction_set.all():
                transaction.user_id = new_user_id
                transaction.save(update_fields={'user_id'})

        self.payment_method = None
        self.save(update_fields={'payment_method'})

    def _on_cancelled(self, update_fields: UpdateFieldsType):
        """Called when the subscription transitions to the 'cancelled' status."""
        self.log.info('Subscription %d was cancelled, setting cancelled_at field too.', self.pk)
        self.cancelled_at = django.utils.timezone.now()

        if update_fields is not None:
            update_fields.add('cancelled_at')

        order = self.latest_order()
        if order and order.status in {'created', 'soft-failed', 'failed'}:
            order.status = 'cancelled'
            order.save(update_fields={'status'})

    def _on_activation_pre_save(self, old_status: str,
                                update_fields: UpdateFieldsType) -> None:
        """Called whenever the subscription is about to become active."""
        if self.started_at is None:
            self.started_at = django.utils.timezone.now()
            if update_fields is not None:
                update_fields.add('started_at')
        self._set_next_payment_after_activation(update_fields)

    def _on_deactivation_pre_save(self, old_status: str,
                                  update_fields: UpdateFieldsType) -> None:
        """Called whenever the subscription is about to become inactive."""

    def _handle_status_change_post_save(self, old_state: typing.Mapping[str, typing.Any]) -> None:
        """Most status change handling should be done here."""
        # Use old_state.get() because there might not be an old state.
        old_status = old_state.get('status', '')
        if old_status == self.status:
            return

        was_active = old_state.get('is_active', False)
        if self.is_active and not was_active:
            self._on_activation_post_save(old_status)
        elif not self.is_active and was_active:
            self._on_deactivation_post_save(old_status)

    def _on_activation_post_save(self, old_status: str) -> None:
        """Called whenever the subscription becomes active."""
        self.log.debug('Subscription pk=%d was activated', self.pk)
        looper.signals.subscription_activated.send(sender=self, old_status=old_status)

    def _on_deactivation_post_save(self, old_status: str) -> None:
        """Called whenever the subscription becomes inactive."""
        self.log.debug('Subscription pk=%d was deactivated', self.pk)
        looper.signals.subscription_deactivated.send(sender=self, old_status=old_status)

    def generate_order(self, *, save=True) -> 'Order':
        """Generate an order for the current subscription.

        :param save: save the generated order to the database.
        """

        # Dynamically get the fields to copy.
        field_copies = {name: getattr(self, name)
                        for name in SharedFields.field_names()}

        customer: Customer = self.user.customer
        order = Order(
            subscription=self,
            email=customer.billing_email,
            billing_address=customer.billing_address_as_text(),
            status='created',
            name=f'{self.plan.product.name} / {self.plan.name}',
            **field_copies,
        )
        if save:
            order.save()
        return order

    def _set_next_payment_after_activation(self, update_fields: UpdateFieldsType):
        """Conditionally set `next_payment` to a suitable value."""
        now = django.utils.timezone.now()
        if self.next_payment is not None and self.next_payment > now:
            self.log.debug('Subscription %r has future next_payment, not bumping after activation.',
                           self.pk)
            return
        self.bump_next_payment()
        if update_fields is not None:
            update_fields.add('next_payment')

    def bump_next_payment(self):
        """Bump the 'next_payment' field to one interval from now.

        Does NOT save this subscription; that's up to the caller.
        """
        renewal = django.utils.timezone.now()
        self.next_payment = renewal + self.interval
        self.log.info('Bumped subscription %r next_payment to %s', self.pk, self.next_payment)

    def __str__(self):
        try:
            plan = self.plan.name
        except Plan.DoesNotExist:
            plan = '(no plan)'
        try:
            customer = self.user.customer.full_name
        except Customer.DoesNotExist:
            customer = '(no customer)'
        return f'{self.id} - {customer} - {plan}'

    def attach_log_entry(self, message: str):
        """Attach an admin history log entry."""
        admin_log.attach_log_entry(self, message)

    def extend_subscription(self, *, from_timestamp: datetime.datetime, months: float) -> None:
        """Extend a subscription by shifting the next_payment date.

        :param from_timestamp: the timestamp to add the number of months to.
        :param months: (possibly fractional) number of months to add.
        """

        # By splitting up fractional months into months + days, paying for one
        # month worth of subscription will just increment the month by one.
        # This means that there is a fixed price per month (so january and
        # feburary cost the same, even though they have different length in
        # days). Any extra days are place on top of that.
        #
        # Personally I (Sybren) think this is gives more intuitive results than
        # converting the entire 'months' into 'months * 30.4' days. With the
        # current implementation paying €50 for a €5-per-month subscription
        # will simply give you 10 months (instead of 304 days).
        entire_months = int(months // 1)
        fractional_month = months % 1
        days = fractional_month * 30.4  # Average 30.4 days per month

        # MyPy doesn't know relativedelta() can handle float days.
        delta = dateutil.relativedelta.relativedelta(  # type: ignore
            months=entire_months, days=days)
        new_next_payment = from_timestamp + delta

        self.log.info('Extending subscription %r next_payment by %.3f months (%s) from %s to %s',
                      self.id, months, delta, self.next_payment, new_next_payment)
        self.next_payment = new_next_payment
        self.save(update_fields={'next_payment'})

    def switch_payment_method(self, payment_method: PaymentMethod) -> None:
        """Also switches collection method if needed.

        Does not influence any orders; that's the caller's responsibility.
        """

        self.log.info('Switching subscription pk=%r to payment method pk=%r',
                      self.pk, payment_method.pk)

        self.payment_method = payment_method
        supported = payment_method.gateway.provider.supported_collection_methods
        if self.collection_method in supported:
            self.save(update_fields={'payment_method'})
            return

        new_method = supported.copy().pop()
        self.attach_log_entry(
            f'Switching collection method from {self.collection_method} to {new_method}'
            f' because the new payment gateway only supports {supported!r}')
        self.log.info('Switching collection method of subscription pk=%r from %r to %r '
                      ' because the new payment gateway only supports %r',
                      self.pk, self.collection_method, new_method, supported)
        self.collection_method = new_method
        self.save(update_fields={'payment_method', 'collection_method'})


class OrderManager(models.Manager):
    def paid(self) -> models.QuerySet:
        """Return only paid orders, so status in 'paid' or 'fulfilled'."""
        return self.filter(status__in={'paid', 'fulfilled'})

    def payable(self) -> models.QuerySet:
        """Return orders that can still be paid."""
        return self.filter(status__in={'created', 'soft-failed'})


# Ignoring type because of the inner Meta class of the two mix-ins.
class Order(model_mixins.RecordModifcationMixin,  # type: ignore
            model_mixins.CreatedUpdatedMixin,
            model_mixins.StateMachineMixin,
            SharedFields,
            models.Model):
    class Meta:
        # We just want to order by creation time, which is reflected in the
        # strictly increasing primary key. Furthermore, that key is already
        # indexed in the database.
        get_latest_by = 'pk'
        ordering = ('-pk',)

    record_modification_fields = {'status'}
    log = log.getChild('Order')

    DEFAULT_STATUS = 'created'
    STATUSES = (
        ('created', 'Created'),
        ('soft-failed', 'Soft-Failed; queued for retrying'),
        ('failed', 'Renewal Failed; manual payment needed'),
        ('paid', 'Paid'),
        ('cancelled', 'Cancelled'),
        ('fulfilled', 'Fulfilled'),
    )

    # Map 'current status': {set of allowed new statuses}
    VALID_STATUS_TRANSITIONS: typing.Dict[str, typing.Set[str]] = {
        'created': {'soft-faled', 'failed', 'paid', 'cancelled'},
        'soft-failed': {'failed', 'paid', 'cancelled'},
        'failed': {'cancelled'},
        'paid': {'fulfilled'},
        'cancelled': set(),
        'fulfilled': set(),
    }

    subscription = models.ForeignKey(Subscription, on_delete=models.CASCADE, editable=False)

    name = models.CharField(
        max_length=255, help_text='What the order is for; shown to the customer')
    email = models.EmailField(max_length=255)  # Snapshot of the current email
    billing_address = models.TextField()  # Snapshot of the current address

    status = models.CharField(choices=STATUSES, default=DEFAULT_STATUS, max_length=20)
    collection_attempts = models.IntegerField(
        default=0, blank=True, editable=False,
        help_text='How often an automatic collection attempt was made.')
    paid_at = models.DateTimeField(
        null=True, blank=True, help_text='When the order was paid, if at all.')

    retry_after = models.DateTimeField(
        null=True, blank=True, help_text='When automatic collection should be retried.')

    objects = OrderManager()

    def latest_transaction(self) -> typing.Optional['Transaction']:
        """Returns the latest transaction for this Order, if there is one"""
        try:
            return self.transaction_set.latest()
        except Transaction.DoesNotExist:
            return None

    def save(self, *args, **kwargs) -> None:
        """Act on state changes."""
        was_changed, old_state = self.pre_save_record()

        old_status = old_state.get('status', '')
        if was_changed \
                and old_status != self.status \
                and self.status == 'paid' \
                and self.paid_at is None:
            self.paid_at = django.utils.timezone.now()
            update_fields: typing.Optional[typing.Set[str]] = kwargs.get('update_fields')
            if update_fields is not None:
                update_fields.add('paid_at')

        super().save(*args, **kwargs)

        if was_changed:
            self._handle_status_change(old_status=old_status)

    def _handle_status_change(self, old_status: str) -> None:
        if old_status == self.status:
            return

        if self.status == 'paid':
            self._on_status_paid(old_status)
        else:
            self.log.debug('Order pk=%d changed status %r to %r', self.pk, old_status, self.status)

    def _on_status_paid(self, old_status: str) -> None:
        """Activate the subscription if possible."""
        if self.subscription.status == 'active':
            return

        if not self.subscription.can_be_activated:
            self.log.info('Order pk=%d changed from %r to %r, but subscription %d cannot '
                          'be activated from subscription status %r',
                          self.pk, old_status, self.status,
                          self.subscription.pk, self.subscription.status)
            return

        self.log.info('Order pk=%d changed from %r to %r, activating subscription %d',
                      self.pk, old_status, self.status, self.subscription.pk)
        self.subscription.status = 'active'
        self.subscription.save(update_fields={'status'})

    def generate_transaction(self, *, save: bool = True) -> 'Transaction':
        """Generate a transaction that allows the customer to pay for the order.

        :param save: The transaction is saved to the database when save=True.
            Otherwise it's up to the caller to do this. This allows simulation
            of the subscription renewal flow.
        """
        transaction = Transaction(
            amount=self.price,
            currency=self.currency,
            user=self.user,
            order=self,
            paid=False,
            payment_method=self.payment_method,
            ip_address=None,  # will be set once the transaction is processed.
        )
        if save:
            transaction.save()
        return transaction

    def __str__(self):
        return f'Order {self.pk} for Subscription {self.subscription.pk}'

    def total_refunded(self) -> Money:
        """Compute the total refunded amount for this order.

        Since an order can have multiple transactions that all have a refunded
        amount, we have to sum them all.

        Note that this function assumes that all transactions have the same
        currency as the order.
        """
        result = self.transaction_set.all().aggregate(models.Sum('amount_refunded'))
        # Prevent None, which happens when an order has no transactions.
        refunded_sum = result['amount_refunded__sum'] or 0
        return Money(self.currency, refunded_sum)

    def attach_log_entry(self, message: str):
        """Attach an admin history log entry to this Order and its Subscription."""
        admin_log.attach_log_entry(self, message)

        admin_url = reverse('admin:looper_order_change', kwargs={'object_id': self.id})
        subs: Subscription = self.subscription  # make PyCharm happy
        subs.attach_log_entry(format_html(
            '{} for <a href="{}">order #{}</a>', message, admin_url, self.id))

    def switch_payment_method(self, payment_method: PaymentMethod) -> None:
        """Also switches collection method if needed.

        Does not influence the subscription; that's the caller's responsibility.
        """

        self.log.info('Switching order pk=%r to payment method pk=%r',
                      self.pk, payment_method.pk)

        self.payment_method = payment_method
        supported = payment_method.gateway.provider.supported_collection_methods
        if self.collection_method in supported:
            self.save(update_fields={'payment_method'})
            return

        new_method = supported.copy().pop()
        self.attach_log_entry(
            f'Switching collection method from {self.collection_method} to {new_method}'
            f' because the new payment gateway only supports {supported!r}')
        self.log.info('Switching collection method of order pk=%r from %r to %r '
                      ' because the new payment gateway only supports %r',
                      self.pk, self.collection_method, new_method, supported)
        self.collection_method = new_method
        self.save(update_fields={'payment_method', 'collection_method'})


class Transaction(model_mixins.CreatedUpdatedMixin, models.Model):
    """Transaction that can be charged or refunded."""
    log = log.getChild('Transaction')

    class Meta:
        # We just want to order by creation time, which is reflected in the
        # strictly increasing primary key. Furthermore, that key is already
        # indexed in the database.
        get_latest_by = 'pk'

    STATUS_DEFAULT = 'pending'
    STATUSES = (
        ('succeeded', 'Succeeded'),
        ('pending', 'Pending'),
        ('failed', 'Failed'),
    )

    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True)
    payment_method = models.ForeignKey(PaymentMethod, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    status = models.CharField(choices=STATUSES, default=STATUS_DEFAULT, max_length=20)
    currency = CurrencyField()
    amount: Money = MoneyField(default=0)
    ip_address = models.GenericIPAddressField(
        null=True,
        help_text='IP address of the user at the moment of paying.')

    # TODO(Sybren): investigate DB constraint that requires both NULL or neither NULL.
    refunded_at = models.DateTimeField(null=True)
    amount_refunded: Money = MoneyField(
        blank=True, default=0,
        help_text='Refunded amount, if any.')

    failure_message = models.CharField(max_length=128, blank=True, default='')
    paid = models.BooleanField()

    transaction_id = models.CharField(
        max_length=128, blank=True, default='',
        help_text='ID of the transaction on the gateway.')

    @property
    def refundable(self) -> Money:
        return self.amount - self.amount_refunded

    def __str__(self):
        return f'Transaction {self.pk} for order {self.order.pk}'

    def save(self, *args, **kwargs) -> None:
        """Act on state changes."""
        is_new = not self.pk
        super().save(*args, **kwargs)

        if is_new:
            self.log.debug('Transaction pk=%d was just created.', self.pk)

    @requires_status('pending')
    def charge(self, *, customer_ip_address: typing.Optional[str]) -> bool:
        """Actually charge the customer.

        :return: True when succesful, False otherwise, in which case `self.status`
            will be set to 'failed' and `self.failure_message` will contain the
            error description.
        """
        assert self.pk, 'Transaction needs to be saved before charging'

        # Save the IP address first, to capture any validation error before
        # we perform the transaction at the gateway.
        self.ip_address = customer_ip_address
        self.save(update_fields={'ip_address'})

        try:
            transaction_id = self.payment_method.gateway.provider.transact_sale(
                self.payment_method.token, self.order.price)
        except looper.exceptions.GatewayError as ex:
            self.log.warning('Transaction pk=%d transact_sale() call failed (%r),'
                             ' failing transaction', self.pk, ex.message)
            self.status = 'failed'
            self.failure_message = ex.with_errors()
            self.save(update_fields={'status', 'failure_message'})
            self.attach_log_entry(f'Charge of {self.amount} failed: {ex!s}')
            return False
        assert isinstance(transaction_id, str), \
            f'transaction_id should be string, not {transaction_id!r}'

        self.log.info('Transaction pk=%d was succesful, storing transaction ID %r',
                      self.pk, transaction_id)

        self.status = 'succeeded'
        self.paid = True
        self.transaction_id = transaction_id
        self.save(update_fields={'status', 'transaction_id', 'paid'})
        self.attach_log_entry(f'Charge of {self.amount} was successful')

        # TODO(Sybren): shouldn't this be done in Transaction.save(), in response
        # to the status changing to 'succeeded'?
        self.order.status = 'paid'
        self.order.save(update_fields={'status'})
        return True

    @requires_status('succeeded')
    def refund(self, amount: Money):
        """Refund the customer.

        :param amount: Amount to refund. May not be more than the total
            transaction amount minus the amount refunded so far.

        :raises looper.exceptions.GatewayError: when the refund could not
            be performed at the payment gateway.
        """
        assert self.pk, 'Transaction needs to be saved before refunding'
        assert isinstance(amount, Money), \
            f'amount must be Money, not {amount!r}'
        if amount.currency != self.currency:
            raise ValueError(f'Refund currency {amount.currency!r} does not match ' \
                             'transaction currency {self.currency!r}')

        try:
            self.payment_method.gateway.provider.refund(self.transaction_id, amount)
        except looper.exceptions.GatewayError as ex:
            raise

        self.log.info('Transaction pk=%d was succesful, storing transaction ID %r',
                      self.pk, self.transaction_id)

        self.refunded_at = django.utils.timezone.now()
        self.amount_refunded = self.amount_refunded + amount
        self.save(update_fields={'refunded_at', 'amount_refunded'})
        self.attach_log_entry(f'Refund of {amount} was successful')

    def attach_log_entry(self, message: str):
        """Attach an admin history log entry to this Transaction and its Order + Subscription."""
        admin_log.attach_log_entry(self, message)

        admin_url = reverse('admin:looper_transaction_change', kwargs={'object_id': self.id})
        order: Order = self.order  # make PyCharm happy
        order.attach_log_entry(format_html(
            '{} for <a href="{}">transaction #{}</a>', message, admin_url, self.id))

# class Coupon(models.Model):
#     """Discounts! Implement later."""
#     pass
#
#
