function blender_notes_startup($) {
    /* Strip off some URL components to get to the
     * <app_name>/<model_name>/<object_id> string we need to construct
     * the /admin/notes/... URL */
    let url_suffix = window.location.pathname.replace('/admin/', '').replace('/change/', '');

    /* Can be called in two ways:
     * $(target).blender_notes_render();
     * $(target).blender_notes_render(html_content);
     *
     * The former invocation fetches HTML, whereas the latter invocation displays that HTML.
     */
    $.fn.blender_notes_render = function (html_response) {
        let $this = this;

        if (typeof html_response == 'undefined') {
            let url = "/admin/notes/" + url_suffix;

            $this.text('↻ Loading notes...');

            $.get(url)
            .done(function(html_response) {
                $this.blender_notes_render(html_response);
            })
            .fail(function(xhr, status, blahi) {
                $this.blender_notes_render(xhr.responseText);
            });
            return;
        }

        this.html(html_response)

        this.find('form').on('submit', function(e) {
            e.preventDefault();

            /* Perform an AJAX POST/DELETE instead of a regular one. */
            let url = e.target.action;
            let method = e.target.getAttribute('method');
            let data = $(e.target).serialize();

            $.ajax({url: url, method: method, data: data})
            .done(function(html_response) {
                $this.blender_notes_render(html_response);
            })
            .fail(function(xhr, status, blahi) {
                $this.blender_notes_render(xhr.responseText);
            });
        })

        return this;
    }

    $(function() {
        /* Only activate when on a 'change-form' admin page. */
        if (!document.body.classList.contains('change-form')) return;

        let notes_div = $('<div>').addClass('blender-notes');
        notes_div.insertAfter($('ul.object-tools'));
        notes_div.blender_notes_render();
    });
};

window.addEventListener("load", function() { blender_notes_startup(django.jQuery); })
