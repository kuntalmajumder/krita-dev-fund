import datetime

import responses
from django.test import override_settings
from django.utils import timezone
from django.contrib.auth import get_user_model
import blender_id_oauth_client.models

from looper.tests import AbstractBaseTestCase, AbstractLooperTestCase
from .. import badges, models

httmock = responses.RequestsMock()
UserModel = get_user_model()


@override_settings(BLENDER_ID={'BASE_URL': 'http://id.local:8000/'})
class AbstractBadgerQueueTest(AbstractLooperTestCase):
    def setUp(self):
        super().setUp()
        blender_id_oauth_client.models.OAuthUserInfo.objects.create(
            user=self.user, oauth_user_id=32747)


class BadgerQueueTest(AbstractBadgerQueueTest):
    def test_url_property(self):
        bqc = models.BadgerQueuedCall(action='grant', user=self.user, badge='über')
        self.assertEqual(
            'http://id.local:8000/api/badger/grant/%C3%BCber/32747',
            bqc.url)

    @httmock.activate
    def test_happy(self):
        httmock.add(
            'POST',
            'http://id.local:8000/api/badger/revoke/there/32747',
            body='', status=204)
        httmock.add(
            'POST',
            'http://id.local:8000/api/badger/grant/hey/32747',
            body='', status=204)

        badges.change_badge(self.user, grant='hey', revoke='there')

        queue = models.BadgerQueuedCall.objects
        self.assertEqual(0, queue.count())

    @httmock.activate
    def test_unreachable_bid(self):
        badges.change_badge(self.user, grant='hey', revoke='there')

        queue = models.BadgerQueuedCall.objects
        self.assertEqual(2, queue.count())

        queued = list(queue.all())
        self.assertEqual(
            'http://id.local:8000/api/badger/revoke/there/32747',
            queued[0].url
        )
        self.assertEqual(
            'http://id.local:8000/api/badger/grant/hey/32747',
            queued[1].url
        )

    @httmock.activate
    def test_bid_500(self):
        nr_of_calls = 0

        def callback(request):
            nonlocal nr_of_calls
            nr_of_calls += 1
            return 500, {}, 'error'

        httmock.add_callback(
            'POST',
            'http://id.local:8000/api/badger/revoke/there/32747',
            callback)
        badges.change_badge(self.user, grant='hey', revoke='there')

        queue = models.BadgerQueuedCall.objects
        self.assertEqual(2, queue.count())

        self.assertEqual(1, nr_of_calls,
                         'After one failed call, the next ones should be immediately queued')


class FlushDelayTest(AbstractBadgerQueueTest):
    """To simplify the test, we use a fake 'now' for evaluation.

    This prevents setting queued_call.created after saving it and saving
    it again.
    """

    def queue_call(self):
        queued_call = models.BadgerQueuedCall(
            action='grant',
            badge='devfund_bronze',
            user=self.user,
            error_code=0,
            error_msg='unit test',
        )
        queued_call.save()

    def test_empty_queue(self):
        flush_delay = badges.flush_delay()
        self.assertIsNone(flush_delay)

    def test_just_queued(self):
        self.queue_call()

        now = timezone.now()
        flush_delay = badges.flush_delay(now=now)
        self.assertEqual(flush_delay, datetime.timedelta(seconds=1))

    def test_queued_1_min_ago(self):
        self.queue_call()

        now = timezone.now() + datetime.timedelta(minutes=1)
        flush_delay = badges.flush_delay(now=now)
        self.assertEqual(flush_delay, datetime.timedelta(seconds=5))

    def test_queued_3_min_ago(self):
        self.queue_call()

        now = timezone.now() + datetime.timedelta(minutes=3)
        flush_delay = badges.flush_delay(now=now)
        self.assertEqual(flush_delay, datetime.timedelta(seconds=10))

    def test_queued_10_min_ago(self):
        self.queue_call()

        now = timezone.now() + datetime.timedelta(minutes=10)
        flush_delay = badges.flush_delay(now=now)
        self.assertEqual(flush_delay, datetime.timedelta(seconds=10))

    def test_queued_1_hour_ago(self):
        self.queue_call()

        now = timezone.now() + datetime.timedelta(hours=1)
        flush_delay = badges.flush_delay(now=now)
        self.assertEqual(flush_delay, datetime.timedelta(seconds=15))


class FlushTimeTest(AbstractBadgerQueueTest):

    def assertCloseTo(self, time1: datetime.datetime, time2: datetime.datetime, **kwargs):
        """Asserts that 'time1' is close to 'time2 + timedelta(**kwargs)'."""
        delta = datetime.timedelta(**kwargs)
        precision = datetime.timedelta(milliseconds=10)

        if time2 + delta - precision <= time1 <= time2 + delta + precision:
            return
        self.fail(f'{time1} is not close to {time2} + {delta}:\n'
                  f'Actual  : {time1}\n'
                  f'Expected: {time2+delta}')

    def queue_call(self, created: datetime.datetime) -> models.BadgerQueuedCall:
        queued_call = models.BadgerQueuedCall(
            action='grant',
            badge='devfund_bronze',
            user=self.user,
            error_code=0,
            error_msg='unit test',
        )
        queued_call.save()
        queued_call.created = created
        queued_call.save(update_fields={'created'})
        return queued_call

    def test_empty_queue(self):
        flush_time = badges.flush_time()
        self.assertIsNone(flush_time)

    def test_just_queued_never_flushed(self):
        now = timezone.now()
        self.queue_call(now)

        flush_time = badges.flush_time(now=now)
        self.assertCloseTo(flush_time, now, seconds=0)

    def test_queued_47_min_ago_flushed_never(self):
        now = timezone.now()
        # Oldest queued call was queued 47 minutes ago.
        self.queue_call(now - datetime.timedelta(minutes=47))

        # Verify that the creation timestamp was set correctly.
        oldest = models.BadgerQueuedCall.objects.order_by('created').first()
        age = now - oldest.created
        self.assertEqual(47 * 60, age.total_seconds())

        # Should be flushed soon.
        flush_time = badges.flush_time(now=now)
        self.assertCloseTo(flush_time, now, seconds=0)

    def test_queued_47_min_ago_flushed_3_sec_ago(self):
        now = timezone.now()
        # Oldest queued call was queued 47 minutes ago.
        oldest = self.queue_call(now - datetime.timedelta(minutes=47))

        # Last flush was 3 seconds ago.
        oldest.last_delivery_attempt = now - datetime.timedelta(seconds=3)
        oldest.save(update_fields={'last_delivery_attempt'})

        self.assertEqual(oldest.last_delivery_attempt, badges.last_delivery_attempt())

        # Should be flushed 15 seconds after last flush.
        flush_delay = badges.flush_delay(now=now)
        self.assertEqual(datetime.timedelta(seconds=15), flush_delay)

        flush_time = badges.flush_time(now=now)
        self.assertCloseTo(flush_time, now, seconds=12)

    def test_more_queued_47_min_ago_flushed_3_sec_ago(self):
        now = timezone.now()
        # Oldest queued call was queued 47 minutes ago.
        oldest = self.queue_call(now - datetime.timedelta(minutes=47))
        # But there are others that are more recent. They should not have an effect.
        self.queue_call(now - datetime.timedelta(minutes=13))
        self.queue_call(now - datetime.timedelta(seconds=10))

        # Last flush was 3 seconds ago.
        oldest.last_delivery_attempt = now - datetime.timedelta(seconds=3)
        oldest.save()

        # Should be flushed 15 seconds after last flush.
        flush_delay = badges.flush_delay(now=now)
        self.assertEqual(datetime.timedelta(seconds=15), flush_delay)

        flush_time = badges.flush_time(now=now)
        self.assertCloseTo(flush_time, now, seconds=12)
