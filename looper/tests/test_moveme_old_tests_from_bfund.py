from django.contrib.auth.models import User
from django.urls import reverse

from . import AbstractBaseTestCase


class BillingAddressEditTestCase(AbstractBaseTestCase):

    def setUp(self):
        self.user = User.objects.create_user('harry', 'harry@blender.org')

    def test_get_billing_endpoint_info(self):
        # Try with non authenticated user
        r = self.client.get(reverse('looper:settings_billing_info'))
        self.assertEqual(r.status_code, 302)
        # Try with authenticated user
        self.client.force_login(self.user)
        r = self.client.get(reverse('looper:settings_billing_info'))
        self.assertEqual(r.status_code, 200)
