from unittest.mock import patch

from django.contrib.auth.models import User
from django.urls import reverse
from django_countries.fields import Country

from looper.gateways import PaymentMethodInfo
from looper.models import Customer, Address, PaymentMethod, Gateway, GatewayCustomerId
from looper.exceptions import GatewayError
from .. import signals
from . import AbstractBaseTestCase


class AbstractSingleUserTestCase(AbstractBaseTestCase):
    fixtures = ['testuser']

    def setUp(self):
        self.user: User = User.objects.get(email='harry@blender.org')
        self.customer: Customer = self.user.customer


class BillingAddressAsTextTest(AbstractSingleUserTestCase):
    def test_just_country(self):
        """Country is a special field, not just a CharField"""

        a = Address(
            street_address='Buikslotermeerplein 161',
            country='NL',
            user=self.user)

        a.save()

        self.assertIsInstance(a.country, Country)
        self.assertEqual('Buikslotermeerplein 161\nNetherlands',
                         self.customer.billing_address_as_text())


class CustomerCreationTestCase(AbstractBaseTestCase):

    def setUp(self):
        self.user = User.objects.create_user('harry', 'harry@blender.org')
        self.customer: Customer = self.user.customer

    def test_customer_creation(self):
        # The Customer should have been created via the post_save signal
        self.assertIsNotNone(self.customer)
        # The default Customer billing_email should match the User email
        self.assertEqual(self.user.email, self.customer.billing_email)

    def test_double_customer_creation(self):
        signals.create_customer(User, self.user, created=True)
        self.user.refresh_from_db()
        self.assertEqual(self.customer.pk, self.user.customer.pk)


class CustomerModelTestCase(AbstractSingleUserTestCase):

    def setUp(self):
        super().setUp()

        # Add default Gateway
        self.gateway = Gateway.objects.create(name='braintree', is_default=True)
        # Isolate Braintree and

    def test_billing_address(self):
        # Ensure the customer does not have an Address associated
        with self.assertRaises(Address.DoesNotExist):
            Address.objects.get(user=self.user)
        # Getting the customer address should always return an Address object
        self.assertIsInstance(self.customer.billing_address, Address)

    def test_payment_method_default_no_method(self):
        # Ensure no payment method is associated to the Customer
        with self.assertRaises(PaymentMethod.DoesNotExist):
            PaymentMethod.objects.get(user=self.user)
        # A customer without payment method should return payment method None
        self.assertIsNone(self.customer.payment_method_default)

    def test_payment_method_default_one_methods(self):
        # Add one default payment method
        first_payment_method = PaymentMethod.objects.create(user=self.user, token='abc',
                                                            gateway=self.gateway)
        # Check that customer.payment_method_default returns that payment method
        self.assertEqual(self.customer.payment_method_default.id, first_payment_method.id)

    def test_payment_method_default_two_methods(self):
        # Add one default payment method
        first_payment_method = PaymentMethod.objects.create(user=self.user, token='abc',
                                                            gateway=self.gateway)
        # Add another payment method
        PaymentMethod.objects.create(user=self.user, token='def', gateway=self.gateway)

        # Getting payment_method_default should still return the first payment method
        self.assertEqual(self.customer.payment_method_default.id, first_payment_method.id)

        # Add another customer
        other_user = User.objects.create_user('ron', 'ron@blender.org')
        other_customer = other_user.customer
        # Add a non default payment method for this customer
        other_payment_method = PaymentMethod.objects.create(user=other_user, token='123',
                                                            gateway=self.gateway)
        # Check that we get the proper payment method
        self.assertEqual(other_customer.payment_method_default.id, other_payment_method.id)

    def test_payment_method_default_deleted_method(self):
        # Create a deleted payment method
        PaymentMethod.objects.create(user=self.user, token='hgr', gateway=self.gateway,
                                     is_deleted=True)
        self.assertIsNone(self.customer.payment_method_default)

    @patch('looper.gateways.BraintreeGateway.payment_method_create')
    def test_double_payment_method(self, mock_payment_method_create):
        # Braintree always returns the same payment method token when the
        # user selected the same payment method, even when the nonces are
        # different, and even when the function called is actually
        # `payment_method.create(...)`.
        mock_payment_method_create.return_value = PaymentMethodInfo('the-same-token-as-before')

        pm1 = self.customer.payment_method_add('nonce-one', self.gateway)
        pm2 = self.customer.payment_method_add('nonce-two', self.gateway)
        self.assertEqual(pm1.pk, pm2.pk)
        self.assertEqual(1, PaymentMethod.objects.count())

    @patch('looper.gateways.BraintreeGateway.customer_create')
    def test_gateway_customer_id_get_or_create(self, mock_customer_create):
        expected_customer_id = '214745181'
        # Mock the response from Braintree when creating a customer
        mock_customer_create.return_value = expected_customer_id

        # No customer exists on the the remote gateway
        with self.assertRaises(GatewayCustomerId.DoesNotExist):
            GatewayCustomerId.objects.get(user=self.user)

        # Create a new gateway customer
        gateway_customer_id = self.customer.gateway_customer_id_get_or_create(self.gateway)
        self.assertEqual(expected_customer_id, gateway_customer_id)
        mock_customer_create.assert_called_with()

        # Check if it exists in the database
        self.assertEqual(expected_customer_id,
                         GatewayCustomerId.objects.get(user=self.user).gateway_customer_id)

    @patch('looper.gateways.BraintreeGateway.customer_create')
    def test_gateway_customer_id_get_or_create_error(self, mock_customer_create):
        # Mock the response as a failure
        mock_customer_create.side_effect = GatewayError(message='mock', errors=['je moeder'])
        with self.assertRaises(GatewayError):
            self.customer.gateway_customer_id_get_or_create(self.gateway)

    @patch('looper.gateways.BraintreeGateway.payment_method_create')
    @patch('looper.gateways.BraintreeGateway.customer_create')
    def test_payment_method_add(self, mock_customer_create, mock_payment_method_create):
        expected_customer_id = '214745181'
        token = 'act'
        mock_customer_create.return_value = expected_customer_id

        pm_info = PaymentMethodInfo(token, PaymentMethodInfo.Type.PAYPAL_ACCOUNT)
        mock_payment_method_create.return_value = pm_info

        pm = self.customer.payment_method_add('fake-valid-nonce', self.gateway)
        self.assertEqual(token, pm.token)
        self.assertEqual('pa', pm.method_type)

    @patch('looper.gateways.BraintreeGateway.payment_method_delete')
    def test_payment_method_delete(self, mock_payment_method_delete):
        mock_payment_method_delete.return_value = None

        pm = PaymentMethod.objects.create(user=self.user, token='hgr', gateway=self.gateway)
        pm.delete()

        pm.refresh_from_db()
        self.assertTrue(pm.is_deleted, 'Payment method should be soft-deleted')
        mock_payment_method_delete.assert_called_with('hgr')

    @patch('looper.gateways.BraintreeGateway.payment_method_delete')
    @patch('looper.gateways.BraintreeGateway.payment_method_create')
    @patch('looper.gateways.BraintreeGateway.customer_create')
    def test_payment_method_add_deleted(self, mock_customer_create, mock_payment_method_create,
                                        mock_payment_method_delete):
        expected_customer_id = '214745181'
        token = 'act'
        mock_customer_create.return_value = expected_customer_id
        mock_payment_method_create.return_value = PaymentMethodInfo(token)
        mock_payment_method_delete.return_value = None

        pm = self.customer.payment_method_add('fake-valid-nonce', self.gateway)
        original_pm_pk = pm.pk
        self.assertEqual(token, pm.token)

        pm.delete()
        pm.refresh_from_db()
        self.assertTrue(pm.is_deleted, 'Payment method should be soft-deleted')
        mock_payment_method_delete.assert_called_with('act')

        new_pm = self.customer.payment_method_add('another-fake-valid-nonce', self.gateway)
        self.assertFalse(new_pm.is_deleted, 'Payment method should not be deleted')
        self.assertEqual(original_pm_pk, new_pm.pk, 'Payment method should have been undeleted')


class CustomerEditTestCase(AbstractSingleUserTestCase):

    def test_customer_form_missing_fields(self):
        pre_changes: dict = self.customer.__dict__.copy()

        self.client.force_login(self.user)
        self.client.post(reverse('looper:settings_personal_info'), data={})

        # The customer shouldn't be updated; values are missing in the posted data.
        self.customer.refresh_from_db()
        self.assertEqual(pre_changes['full_name'], self.customer.full_name)
        self.assertEqual(pre_changes['billing_email'], self.customer.billing_email)
        self.assertEqual(pre_changes['company'], self.customer.company)

    def test_customer_form_success(self):
        self.client.force_login(self.user)
        payload = {
            'full_name': 'Henrietta de Aaier',
            'billing_email': 'henrietta+billing@example.com',
            'company': '',
        }
        r = self.client.post(reverse('looper:settings_personal_info'), data=payload)
        self.assertEqual(302, r.status_code, 'After POST the page should redirect')
        self.assertEqual(r['Location'], reverse('looper:settings_personal_info'))

        # The customer should have been updated.
        self.customer.refresh_from_db()
        self.assertEqual(payload['full_name'], self.customer.full_name)
        self.assertEqual(payload['billing_email'], self.customer.billing_email)
        self.assertEqual(payload['company'], self.customer.company)

        # These fields weren't part of the form, so should keep their initial value.
        self.assertEqual('NL11ABCDEFQQQQ', self.customer.vat_number)
        self.assertEqual(True, self.customer.tax_exempt)


class BillingAddressEditTestCase(AbstractSingleUserTestCase):

    def setUp(self):
        super().setUp()

        ba: Address = self.customer.billing_address
        ba.company = self.customer.company
        ba.country = 'NL'
        ba.street_address = 'Beukslotermeerplein 161'
        ba.save()

    def test_billing_address_form_missing_fields(self):
        pre_changes: str = self.customer.billing_address_as_text()

        self.client.force_login(self.user)
        self.client.post(reverse('looper:settings_billing_info'), data={})

        self.customer.refresh_from_db()
        self.customer.billing_address.refresh_from_db()
        self.assertEqual(pre_changes, self.customer.billing_address_as_text())

    def test_billing_address_form_success(self):
        payload = {
            'full_name': 'Гарри Поттер',
            'company': 'Министерство Магии',
            'street_address': 'Scotland Pl',
            'locality': 'London',
            'postal_code': 'SW1A 2BD',
            'region': 'Westminster',
            'country': 'GB',
        }
        ba: Address = self.customer.billing_address

        # Check that all original address fields are different from the payload we will submit
        for name, not_expect in payload.items():
            self.assertNotEqual(not_expect, getattr(ba, name))

        self.client.force_login(self.user)
        r = self.client.post(reverse('looper:settings_billing_info'), data=payload)
        self.assertEqual(302, r.status_code, 'After POST the page should redirect')
        self.assertEqual(r['Location'], reverse('looper:settings_billing_info'))

        # Check that all address fields are now matching the payload submitted
        ba.refresh_from_db()
        for name, expect in payload.items():
            self.assertEqual(expect, getattr(ba, name), r.content.decode())
