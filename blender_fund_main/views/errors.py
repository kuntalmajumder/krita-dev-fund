import logging

from django.conf import settings
from django.views.generic import TemplateView

log = logging.getLogger(__name__)


class ErrorHandler500(TemplateView):
    template_name = 'errors/error500.html'

    log = log.getChild('ErrorHandler500')

    def get(self, *args, **kwargs):
        self.log.exception('Uncaught exception, going to return a 500 Internal Server Error')
        return super().get(*args, **kwargs)

    def get_context_data(self, **kwargs) -> dict:
        import sys
        (ex_type, ex_value, ex_traceback) = sys.exc_info()

        return {
            **super().get_context_data(**kwargs),
            'ex_type': getattr(ex_type, '__name__', str(ex_type)),
            'ex_value': ex_value,
            'ex_message': str(ex_value),
        }


def test_error_500(request):
    if settings.DEBUG:
        # When in debug mode just raising an exception will show
        # the debug screen, and not call the error handler. We have
        # to do that ourselves.
        try:
            raise NotImplementedError('this is an exception that should cause a 500 error')
        except NotImplementedError:
            # Call the handler from within an exception handler,
            # to ensure sys.exc_info() returns something useful.
            return ErrorHandler500.as_view()(request)
    else:
        raise NotImplementedError('this is an exception that should cause a 500 error')
