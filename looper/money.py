import functools
import typing

import babel.numbers
from django.conf import settings


class CurrencyMismatch(ValueError):
    """Raised when mixing currencies in one mathematical expression."""


@functools.total_ordering
class Money:
    """Immutable class for monetary amounts."""
    __slots__ = ('_currency', '_cents')

    def __init__(self, currency: str, cents: int) -> None:
        if not isinstance(currency, str):
            raise TypeError(f'currency must be string, not {type(currency)}')
        if not isinstance(cents, int):
            raise TypeError(f'cents must be integer, not {type(cents)}')

        self._currency = currency
        self._cents = cents

    @property
    def currency(self) -> str:
        return self._currency

    @property
    def cents(self) -> int:
        return self._cents

    @property
    def just_cents(self) -> int:
        """Return just the cents, so € 1.23 → 23"""
        return self._cents % 100

    @property
    def just_whole(self) -> int:
        """Return the whole currency, so € 1.23 → 1"""
        return self._cents // 100

    @property
    def currency_symbol(self) -> str:
        return babel.numbers.get_currency_symbol(self._currency,
                                                 locale=settings.LOOPER_MONEY_LOCALE)

    @property
    def decimals_string(self) -> str:
        """Return the amount as a string, without currency, and with a decimal point.

        >>> Money('EUR', 1033).decimals_string
        '10.33'
        """
        return f'{self.just_whole}.{self.just_cents:02}'

    def __str__(self) -> str:
        return f'{self._currency}\u00A0{self.decimals_string}'

    @classmethod
    def from_str(cls, as_str: str) -> 'Money':
        """Reverse what the __str__() method produces into a Money instance.

        str(money) is called by the Django serialiser, for example when dumping
        data into fixtures. This function is used to undo that, and parse the
        string back into a Money instance.
        """
        try:
            currency, decimals_string = as_str.split('\u00A0', 1)
        except ValueError:
            raise ValueError('from_str() expects a non-breaking space as separator') from None
        cents = int(round(float(decimals_string) * 100))
        return Money(currency, cents)

    def __repr__(self) -> str:
        return f'Money(currency={self._currency!r}, cents={self._cents})'

    def with_currency_symbol(self) -> str:
        """Return the amount as string with currency symbol, so '€' instead of 'EUR'."""
        return f'{self.currency_symbol}\u00A0{self.decimals_string}'

    def with_currency_symbol_rounded(self) -> str:
        """Same as with_currency_symbol() rounding to entire units."""

        whole_rounded = int(round(self._cents / 100))
        return f'{self.currency_symbol}\u00A0{whole_rounded}'

    def with_currency_symbol_nonocents(self) -> str:
        """Same as with_currency_symbols but never ends in '.00'."""

        whole, cents = divmod(self._cents, 100)
        if cents:
            return f'{self.currency_symbol}\u00A0{whole}.{cents:02}'
        return f'{self.currency_symbol}\u00A0{whole}'

    def __pos__(self) -> 'Money':
        return Money(self._currency, self._cents)

    def __neg__(self) -> 'Money':
        return Money(self._currency, -self._cents)

    def _assert_same_currency(self, other) -> None:
        if not isinstance(other, Money):
            raise TypeError(f'{other!r} is not {Money!r}')
        if self._currency == other._currency:
            return
        raise CurrencyMismatch(f'Currency mismatch between {self} and {other}')

    def __add__(self, other: 'Money') -> 'Money':
        self._assert_same_currency(other)
        return Money(self._currency, self._cents + other._cents)

    def __sub__(self, other: 'Money') -> 'Money':
        self._assert_same_currency(other)
        return Money(self._currency, self._cents - other._cents)

    def __mul__(self, other) -> 'Money':
        if isinstance(other, Money):
            raise TypeError('cannot multiply monetary quantities')
        if not isinstance(other, int):
            raise TypeError(f'unsupported type {type(other)}')

        return Money(self._currency, self._cents * other)

    __radd__ = __add__
    __rmul__ = __mul__

    def __truediv__(self, divisor: typing.Union[int, 'Money']) -> typing.Union[list, float]:
        """Split up the amount in (almost-)equal parts or compute ratio.

        When the divisor is an integer, return a list of `divisor` Money
        objects that sum to `self`.

        When the divisor is a Money object, return the ratio between 'self'
        and 'divisor'. This requires both Money objects to have the same
        currency.

        To prevent money loss and integer cent precision, something like
        `Money('EUR', 10) / 3` should return three Money objects with resp.
        4, 3, and 3 cents.
        """
        if isinstance(divisor, Money):
            return self._ratio(divisor)
        if not isinstance(divisor, int):
            raise TypeError(f'Money can only be divided by Money or integer, not {divisor!r}')
        if divisor < 0:
            raise ValueError('Unable to divide by negative amount')
        if divisor == 0:
            raise ZeroDivisionError()

        remainder = abs(self._cents)
        sign = 1 if self._cents >= 0 else -1
        results = []
        for_all_parts = remainder // divisor
        for _ in range(divisor):
            results.append(Money(self._currency, sign * for_all_parts))
            remainder -= for_all_parts

        # Spread the remainder more or less equally
        assert remainder < divisor
        for i in range(remainder):
            results[i]._cents += sign

        return results

    def __floordiv__(self, divisor: typing.Union[int, float]) -> 'Money':
        """Lossy division of the money.

        Returns the highest amount in integer cents for which
        `result * divisor <= self` holds.

        Note that this should NOT BE USED to divide a monetary amount over
        multiple recipients, as it WILL INCUR LOSSES.
        """
        if not isinstance(divisor, (int, float)):
            raise TypeError(f'Money can only be divided by an integer or float, not {divisor!r}')
        if divisor < 0:
            raise ValueError('Unable to divide by negative amount')
        if divisor == 0:
            raise ZeroDivisionError()

        divided_cents = int(self._cents // divisor)
        return self.__class__(self._currency, divided_cents)

    def _ratio(self, other: 'Money') -> float:
        self._assert_same_currency(other)
        return self.cents / other.cents

    def __eq__(self, other) -> bool:
        if not isinstance(other, Money):
            return False
        return self._cents == other._cents and self._currency == other._currency

    def __ne__(self, other) -> bool:
        return not self.__eq__(other)

    def __hash__(self) -> int:
        return hash((self._currency, self._cents))

    def __lt__(self, other):
        if not isinstance(other, Money):
            return NotImplemented
        self._assert_same_currency(other)
        return self._cents < other._cents

    def __bool__(self) -> bool:
        return self._cents != 0


def sum_per_currency(amounts: typing.Iterable[Money]) -> typing.Mapping[str, Money]:
    """Compute the sum of all given amounts, separated by currency.

    :return: Mapping of currency to the sum for that currency.
    """
    import collections

    total_per_currency: typing.Dict[str, int] = collections.defaultdict(int)

    for amount in amounts:
        total_per_currency[amount.currency] += amount.cents

    return {
        currency: Money(currency, sum_cents)
        for currency, sum_cents in total_per_currency.items()
    }


def sum_to_euros(amounts: typing.Iterable[Money]) -> Money:
    """Sums all amounts while converting to €.

    Note that all converted amounts are rounded to entire cents, so
    this is a lossy operation.
    """

    from django.conf import settings

    rates = settings.LOOPER_CONVERTION_RATES_FROM_EURO

    sum_cents_eur = 0
    for amount in amounts:
        conversion_rate = rates[amount.currency]
        converted_cents = int(round(amount.cents / conversion_rate))
        sum_cents_eur += converted_cents

    return Money('EUR', sum_cents_eur)


def convert_currency(amount: Money, *, to_currency: str) -> Money:
    """Convert the amount to another currency.

    Note that the converted amount is rounded to entire cents, so
    this is a lossy operation.
    """
    from django.conf import settings

    if amount.currency == to_currency:
        return amount

    rates = settings.LOOPER_CONVERTION_RATES_FROM_EURO

    current_to_euro = 1 / rates[amount.currency]
    euro_to_target = rates[to_currency]

    cents_in_eur = amount.cents * current_to_euro
    cents_in_target = cents_in_eur * euro_to_target
    converted_cents = int(round(cents_in_target))

    return Money(to_currency, converted_cents)
