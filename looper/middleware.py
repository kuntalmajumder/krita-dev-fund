import functools
import logging
import threading
import typing

import geoip2.database
from django.conf import settings
from geoip2.errors import AddressNotFoundError

from . import utils

PREFERRED_CURRENCY_SESSION_KEY = 'PREFERRED_CURRENCY'

SKIP_GEO_LOOKUP = {'127.0.0.1', '::1', 'unknown', ''}
"""Addresses for which country lookups should be skipped."""

log = logging.getLogger(__name__)

_thread_local = threading.local()
"""Thread-local storage containing the remote addr for the current request."""


class RemoteAddressRecorder:
    """Store the request's remote IP address for later retrieval.

    By using this middleware, code deeper into the call stack can access
    the remote address of the request without having to pass the request
    itself to it.
    """

    def __init__(self, get_response: typing.Callable) -> None:
        self.get_response = get_response

    def __call__(self, request):
        _thread_local.remote_addr = request.META.get('REMOTE_ADDR') or ''
        try:
            response = self.get_response(request)
        finally:
            _thread_local.remote_addr = ''
        return response


class PreferredCurrencyMiddleware:
    """Expose preferred currency in request.session[PREFERRED_CURRENCY_SESSION_KEY].

    Very simple selector; if the country is in the European Union, we assume
    the Euro is preferred, otherwise the US Dollar is chosen.

    Uses the session, so install after django.contrib.sessions.middleware.SessionMiddleware.
    """
    log = log.getChild('PreferredCurrencyMiddleware')

    def __init__(self, get_response) -> None:
        self.log.info('Opening GeoIP2 database %s', settings.GEOIP2_DB)
        self.geoip = geoip2.database.Reader(settings.GEOIP2_DB)
        self.get_response = get_response

    def __call__(self, request):
        self.update_session(request)
        return self.get_response(request)

    @functools.lru_cache(maxsize=128)
    def lookup_country(self, remote_addr: str):
        if remote_addr in SKIP_GEO_LOOKUP:
            # We already know that the below call will fail; don't bother
            # even trying to look up the address.
            return None

        try:
            country = self.geoip.country(remote_addr)
        except AddressNotFoundError:
            return None
        except OSError as ex:
            self.log.warning('Unable to look up remote address %r in GeoIP database: %s',
                             remote_addr, ex)
            return None
        return country.country

    def preferred_currency(self, country) -> str:
        """Determine default currency for the given country.

        Unfortunately geoip2.models.Country isn't defined in a MyPy-compatible
        way, so we can't properly declare its type.

        :type country: geoip2.models.Country
        """
        if country is None:
            return 'USD'
        if country.is_in_european_union:
            return 'EUR'
        return 'USD'

    def update_session(self, request):
        if not hasattr(request, 'session'):
            self.log.warning('Session middleware not available, unable to set preferred currency')
            return

        if PREFERRED_CURRENCY_SESSION_KEY in request.session:
            # Preferred currency is already set; don't overwrite it.
            return

        remote_addr = utils.get_client_ip(request)
        country = self.lookup_country(remote_addr)
        currency = self.preferred_currency(country)
        request.session[PREFERRED_CURRENCY_SESSION_KEY] = currency
