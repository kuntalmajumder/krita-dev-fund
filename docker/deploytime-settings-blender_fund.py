"""
Deploy-time settings.

These settings are used during the preparation of the Docker image,
so that we can run `manage.py collectstatic`. The settings should
mimick the production settings, but of course not contain any secrets.

The actual settings should be mounted using a Docker volume.
"""

# noinspection PyUnresolvedReferences
from blender_fund.settings_common import *

DEBUG = False
SECRET_KEY = r'''1234'''


import braintree

GATEWAYS = {
    'braintree': {
        'environment': braintree.Environment.Sandbox,
        # Note that this is something else than 'Merchant Account ID':
        'merchant_id': 'aaaaaaaaaaaaaaaa',
        'public_key': 'aaaaaaaaaaaaaaaa',
        'private_key': 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',

        # Merchant Account IDs for different currencies.
        # Configured in Braintree: Account → Merchant Account Info.
        'merchant_account_ids': {
            'EUR': 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'USD': 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb',
        }
    },
    'bank': {},
}

import sys
import os

if os.path.exists('/var/www/settings/blender_fund_settings.py'):
    sys.path.append('/var/www/settings')
    from blender_fund_settings import *
