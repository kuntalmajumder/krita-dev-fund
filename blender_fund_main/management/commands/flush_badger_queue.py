import logging
import time

from django.core.management.base import BaseCommand
from django.utils import timezone

from blender_fund_main import badges

log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Flushes the Badger API queue'

    def add_arguments(self, parser):
        parser.add_argument('--flush', '-f',
                            action='store_true',
                            default=False,
                            help='Really perform the flush; without this only the '
                                 'size of the queue is shown.')
        parser.add_argument('--monitor', '-m',
                            action='store_true',
                            default=False,
                            help='Continually monitors the Badger API queue.')

    def handle(self, *args, **options):
        do_flush = options['flush']
        verbose = options['verbosity'] > 0

        levels = {
            0: logging.ERROR,
            1: logging.WARNING,
            2: logging.INFO,
            3: logging.DEBUG,
        }
        level = levels.get(options['verbosity'], logging.DEBUG)
        logging.getLogger('blender_fund_main').setLevel(level)
        logging.disable(level - 1)
        if options['monitor']:
            return self.monitor()

        count = badges.queue_size()
        if verbose:
            if count == 0:
                self.stdout.write(self.style.SUCCESS(f'The queue is empty'))
            elif count == 1:
                self.stdout.write(self.style.WARNING(f'There is 1 queued item'))
            else:
                self.stdout.write(self.style.WARNING(f'There are {count} queued items'))

        if not do_flush:
            if count > 0:
                self.stdout.write('Use the --flush CLI option to perform flush.')
            return

        if verbose:
            self.stdout.write('Flushing...')
        badges.flush()
        if verbose:
            self.stdout.write('Flush performed...')

            new_count = badges.queue_size()
            if new_count == 0:
                self.stdout.write(self.style.SUCCESS('The queue is now empty'))
            elif new_count == 1:
                self.stdout.write(self.style.WARNING(f'There is still 1 item queued.'))
            else:
                self.stdout.write(self.style.WARNING(f'There are still {new_count} items queued.'))

    def monitor(self):
        """Keeps monitoring the queue."""

        try:
            while True:
                self._monitor_iteration()
                time.sleep(1)
        except KeyboardInterrupt:
            log.info('shutting down webhook queue monitor')

    def _monitor_iteration(self):
        """Single iteration of queue monitor."""

        # Quick check to see if any hook got anything queued.
        queue_size = badges.queue_size()
        if queue_size == 0:
            log.debug('nothing queued')
            return

        # Only flush when flushing is necessary.
        flush_time = badges.flush_time()

        if flush_time is None:  # Means that there is nothing queued.
            return
        secs_in_future = (flush_time - timezone.now()).total_seconds()
        if secs_in_future > 1:
            log.debug('skipping, flush time is %d seconds in future', secs_in_future)
            return

        badges.flush()
