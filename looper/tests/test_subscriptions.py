import datetime
import logging
import typing
from unittest import mock

from django.dispatch import receiver
import django.utils.timezone
from django.urls import reverse

from looper.middleware import PREFERRED_CURRENCY_SESSION_KEY
from . import AbstractLooperTestCase
from .. import forms
from ..money import Money
from ..models import Subscription
from ..exceptions import IncorrectStatusError

log = logging.getLogger(__name__)


class SubscriptionsTest(AbstractLooperTestCase):
    def test_subscription_activation_signal(self):
        from ..signals import subscription_activated

        signal_old_status = ''
        signal_new_status = ''
        signal_count = 0

        @receiver(subscription_activated)
        def activated(sender: Subscription, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_count
            signal_old_status = old_status
            signal_new_status = sender.status
            signal_count += 1

        subs = self.create_subscription()
        self.assertFalse(subs.is_active)

        now = django.utils.timezone.now()
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = now
            subs.status = 'active'
            subs.save(update_fields={'status'})

        subs.refresh_from_db()
        self.assertTrue(subs.is_active)
        self.assertAlmostEqualDateTime(now, subs.started_at)
        self.assertEqual(1, signal_count)
        self.assertEqual('on-hold', signal_old_status)
        self.assertEqual('active', signal_new_status)

        # Saving with status='active' again shouldn't trigger another signal.
        subs.save(force_update=True)
        self.assertEqual(1, signal_count)

    def test_subscription_deactivation_signal(self):
        from ..signals import subscription_deactivated

        signal_old_status = ''
        signal_new_status = ''
        signal_count = 0

        @receiver(subscription_deactivated)
        def deactivated(sender: Subscription, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_count
            signal_old_status = old_status
            signal_new_status = sender.status
            signal_count += 1

        subs = self.create_subscription()
        subs.status = 'active'
        subs.save(update_fields={'status'})
        subs.status = 'cancelled'
        subs.save(update_fields={'status'})

        subs.refresh_from_db()
        self.assertFalse(subs.is_active)
        self.assertEqual(1, signal_count)
        self.assertEqual('active', signal_old_status)
        self.assertEqual('cancelled', signal_new_status)

    def test_subscription_pending_cancellation(self):
        from ..signals import subscription_deactivated

        signal_count = 0

        @receiver(subscription_deactivated)
        def deactivated(*args, **kwargs):
            nonlocal signal_count
            signal_count += 1

        subs = self.create_subscription()
        subs.status = 'active'
        subs.save(update_fields={'status'})
        subs.status = 'pending-cancellation'
        subs.save(update_fields={'status'})

        subs.refresh_from_db()
        self.assertTrue(subs.is_active)

        # This should *not* send the deactivation signal, as the subscription
        # is still active.
        self.assertEqual(0, signal_count)

    def test_create_managed_subscription(self):
        subscription = Subscription.objects.create(
            plan=self.plan,
            user=self.user,
            price=self.planvar.price,
            currency=self.planvar.currency,
            payment_method=None,
            collection_method='managed',
            interval_unit=self.planvar.interval_unit,
            interval_length=self.planvar.interval_length,
        )
        self.assertEqual('on-hold', subscription.status)
        self.assertEqual(0, subscription.order_set.count())
        self.assertIsNone(subscription.latest_order())


class SubscriptionCancellationTest(AbstractLooperTestCase):
    log = log.getChild('SubscriptionCancellationTest')

    def cancel(self, subscription: Subscription) -> datetime.datetime:
        log.debug('Cancelling subscription pk=%d', subscription.pk)
        now = django.utils.timezone.now()
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.side_effect = [now]
            subscription.cancel_subscription()

        return now

    def test_cancel_while_on_hold(self):
        subs = self.create_subscription()
        order = subs.generate_order()
        order.status = 'failed'
        order.save()
        self.assertEqual('on-hold', subs.status)

        cancelled_at = self.cancel(subs)

        subs.refresh_from_db()
        order = subs.latest_order()
        self.assertEqual('cancelled', subs.status)
        self.assertEqual(cancelled_at, subs.cancelled_at)
        self.assertEqual('cancelled', order.status)

    def test_cancel_while_active(self):
        subs = self.create_active_subscription()
        self.cancel(subs)

        subs.refresh_from_db()
        order = subs.latest_order()
        self.assertEqual('pending-cancellation', subs.status)
        self.assertIsNone(subs.cancelled_at)
        self.assertEqual('paid', order.status)

    def test_cancel_while_pending_cancellation(self):
        subs = self.create_active_subscription()
        self.cancel(subs)

        subs.refresh_from_db()
        self.assertEqual('pending-cancellation', subs.status)

        with self.assertRaises(IncorrectStatusError):
            self.cancel(subs)

        subs.refresh_from_db()
        self.assertEqual('pending-cancellation', subs.status)
        self.assertIsNone(subs.cancelled_at)

    def test_cancel_while_cancelled(self):
        subs = self.create_subscription()
        subs.status = 'cancelled'
        subs.save()

        with self.assertRaises(IncorrectStatusError):
            self.cancel(subs)

        subs.refresh_from_db()
        self.assertEqual('cancelled', subs.status)

    def test_undo_pending_cancellation(self):
        subs = self.create_active_subscription()
        self.cancel(subs)

        subs.refresh_from_db()
        self.assertEqual('pending-cancellation', subs.status)

        subs.status = 'active'
        subs.save()

        subs.refresh_from_db()
        self.assertEqual('active', subs.status)
        self.assertIsNone(subs.cancelled_at)
        self.assertEqual('paid', subs.latest_order().status)


class NextPaymentTest(AbstractLooperTestCase):

    def test_after_creation(self):
        subs = self.create_subscription()
        self.assertFalse(subs.is_active)
        self.assertIsNone(subs.next_payment)

    def test_mid_month(self):
        subs = self.create_subscription()

        now = datetime.datetime(2018, 9, 12, 11, 59, 3, tzinfo=django.utils.timezone.utc)
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = now
            subs.status = 'active'
            subs.save(update_fields={'status'})

        expect_renewal = datetime.datetime(2018, 10, 12, 11, 59, 3,
                                           tzinfo=django.utils.timezone.utc)
        self.assertAlmostEqualDateTime(expect_renewal, subs.next_payment)

    def test_end_of_long_month_before_leapyear(self):
        subs = self.create_subscription()

        now = datetime.datetime(2016, 1, 31, 11, 59, 3, tzinfo=django.utils.timezone.utc)
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = now
            subs.status = 'active'
            subs.save(update_fields={'status'})

        expect_renewal = datetime.datetime(2016, 2, 29, 11, 59, 3,
                                           tzinfo=django.utils.timezone.utc)
        self.assertAlmostEqualDateTime(expect_renewal, subs.next_payment)


class MonthlyPriceTest(AbstractLooperTestCase):
    def price(self, **kwargs) -> Money:
        subscription = Subscription(currency='EUR', price=1000, **kwargs)
        return subscription.monthly_rounded_price

    def test_daily(self):
        self.assertEqual(Money('EUR', 6000), self.price(interval_unit='day', interval_length=5))
        self.assertEqual(Money('EUR', 2308), self.price(interval_unit='day', interval_length=13))
        self.assertEqual(Money('EUR', 750), self.price(interval_unit='day', interval_length=40))

    def test_weekly(self):
        self.assertEqual(Money('EUR', 4286), self.price(interval_unit='week', interval_length=1))
        self.assertEqual(Money('EUR', 330), self.price(interval_unit='week', interval_length=13))

    def test_monthly(self):
        self.assertEqual(Money('EUR', 1000), self.price(interval_unit='month', interval_length=1))
        self.assertEqual(Money('EUR', 250), self.price(interval_unit='month', interval_length=4))

    def test_yearly(self):
        self.assertEqual(Money('EUR', 83), self.price(interval_unit='year', interval_length=1))
        self.assertEqual(Money('EUR', 5), self.price(interval_unit='year', interval_length=16))
