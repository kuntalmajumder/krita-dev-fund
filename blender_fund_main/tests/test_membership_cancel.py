import logging

import django.core.mail
from django.dispatch import receiver
from django.urls import reverse

from looper.tests import AbstractLooperTestCase
from .. import models, signals

log = logging.getLogger(__name__)


class MembershipActivationSignalTest(AbstractLooperTestCase):
    fixtures = AbstractLooperTestCase.fixtures + ['default_site', 'systemuser']

    def cancel_membership(self, mem: models.Membership):
        signal_old_status = ''
        signal_new_status = ''
        signal_count = 0

        @receiver(signals.membership_deactivated)
        def deactivated(sender: models.Membership, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_count
            signal_old_status = old_status
            signal_new_status = sender.status
            signal_count += 1

        # Go through the typical flow of cancelling a membership.
        self.client.force_login(self.user)
        cancel_url = reverse('settings_membership_cancel', kwargs={'membership_id': mem.pk})
        self.assertEqual(200, self.client.get(cancel_url).status_code)

        # This shouldn't do anything.
        resp = self.client.post(cancel_url, data={'confirm': False})
        self.assertEqual(200, resp.status_code)
        mem.refresh_from_db()

        # This should actually cancel the membership.
        log.info('Cancelling membership %d', mem.pk)
        resp = self.client.post(cancel_url, data={'confirm': True})
        self.assertEqual(302, resp.status_code)
        edit_url = reverse('settings_membership_edit', kwargs={'membership_id': mem.pk})
        self.assertEqual(edit_url, resp['Location'], 'Expected redirect to membership edit page')

        mem.refresh_from_db()
        return signal_count, signal_old_status, signal_new_status

    def test_cancel_active_without_subscription(self):
        mem = models.Membership.objects.create(
            level_id=1,
            status='active',
            display_name='je moeder',
            user=self.user,
        )

        # An email should have been sent to the owner of the membership.
        self.assertEqual(1, len(django.core.mail.outbox))

        signal_count, signal_old_status, signal_new_status = self.cancel_membership(mem)
        self.assertEqual('inactive', mem.status)

        # A signal about the deactivation should have been sent.
        self.assertEqual(1, signal_count)
        self.assertEqual('active', signal_old_status)
        self.assertEqual('inactive', signal_new_status)

        # Another email should have been sent to the owner of the membership.
        self.assertEqual(2, len(django.core.mail.outbox))

    def assert_mail_ok(self):
        the_mail: django.core.mail.message.EmailMultiAlternatives = django.core.mail.outbox[-1]
        self.assertIn(self.user.customer.full_name, the_mail.body)
        alt0_body, alt0_type = the_mail.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')

    def test_cancel_active_with_subscription(self):
        subs = self.create_active_subscription()
        mem = self.user.memberships.first()

        # An email should have been sent to the owner of the membership.
        self.assertEqual(1, len(django.core.mail.outbox))
        signal_count, *_ = self.cancel_membership(mem)

        # This should not have cancelled the membership, as the subscription
        # goes to pending-cancellation.
        self.assertEqual('active', mem.status)
        self.assertEqual(0, signal_count)
        self.assertEqual(1, len(django.core.mail.outbox))
        self.assertEqual(subs.pk, mem.subscription.pk)
        self.assertEqual('pending-cancellation', mem.subscription.status)

    def test_cancel_unpaid_subscription(self):
        subs = self.create_on_hold_subscription()

        mem: models.Membership = self.user.memberships.first()

        self.cancel_membership(mem)
        self.assertEqual('inactive', mem.status)
        self.assertEqual(subs.pk, mem.subscription.pk)
        self.assertEqual('cancelled', mem.subscription.status)

        # An email should have been sent to the owner of the membership.
        self.assertEqual(2, len(django.core.mail.outbox))
        self.assert_mail_ok()
