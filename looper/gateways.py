import abc
import enum
import logging
import typing

import attr
import braintree
import braintree.resource
from django.conf import settings

from looper.money import Money
import looper.exceptions

log = logging.getLogger(__name__)

MaybeConfiguredGateway = typing.Union['AbstractPaymentGateway', 'GatewayNotConfigured']


class GatewayNotConfigured:
    """Dummy class that indicates a payment provider gateway isn't configured."""


class BraintreeError(looper.exceptions.GatewayError):
    import braintree.attribute_getter
    import braintree.error_result

    def __init__(self, braintree_error: braintree.error_result.ErrorResult) -> None:
        """Convert Braintree-specific errors into strings.

        Braintree returns errors as strings (i.e. ending in a period) which
        makes composition more difficult, so we strip trailing periods.
        """

        message = braintree_error.message.rstrip('.')
        super().__init__(
            message,
            (self._nice_message(err, idx, message)
             for idx, err in enumerate(braintree_error.errors.deep_errors)),
        )

    @staticmethod
    def _nice_message(braintree_error: braintree.attribute_getter.AttributeGetter,
                      error_index: int,
                      base_message: str) -> str:
        parts = [getattr(braintree_error, 'message', 'unknown error').rstrip('.')]
        if hasattr(braintree_error, 'code'):
            code = braintree_error.code

            if error_index == 0 and parts[0] == base_message:
                # This is common with Braintree, f.e. base_message='Payment method token is invalid'
                # and parts[0]='Payment method token is invalid'. In that case we just include
                # the error code and avoid repeating the same message.
                return f'code {code}'

            parts.append(f'(code {code})')
        return ' '.join(parts)


@attr.s(auto_attribs=True)
class PaymentMethodInfo:
    class Type(enum.Enum):
        UNSUPPORTED = 'unsupported'
        CREDIT_CARD = 'credit_card'
        PAYPAL_ACCOUNT = 'paypal_account'
        BANK_ACCOUNT = 'bank_account'

    token: str
    method_type: Type = Type.UNSUPPORTED

    # Only for known payment types ('credit_card' and 'paypal_account')
    image_url: str = ''

    # Only when method_type = 'credit_card'
    last_4: str = ''
    expiration_date: str = ''  # probably 'MM/YYYY'
    card_type: str = ''

    # Only when method_type = 'paypal_account'
    email: str = ''

    _log = log.getChild('PaymentMethodInfo')

    @classmethod
    def from_braintree(cls, payment_method: braintree.resource.Resource) -> 'PaymentMethodInfo':
        info = PaymentMethodInfo(payment_method.token)
        if isinstance(payment_method, braintree.CreditCard):
            info.method_type = cls.Type.CREDIT_CARD
            info.last_4 = payment_method.last_4
            info.expiration_date = payment_method.expiration_date
            info.image_url = payment_method.image_url
            info.card_type = payment_method.card_type
        elif isinstance(payment_method, braintree.PayPalAccount):
            info.method_type = cls.Type.PAYPAL_ACCOUNT
            info.email = payment_method.email
            info.image_url = payment_method.image_url
        else:
            cls._log.warning('Payment method %r is of unsupported class %r',
                             payment_method, payment_method.__class__)
        return info

    def recognisable_name(self) -> str:
        """Construct a name so that customers can recognise the payment method."""
        if self.method_type == self.Type.CREDIT_CARD:
            return f'{self.card_type} credit card ending in {self.last_4}'
        if self.method_type == self.Type.PAYPAL_ACCOUNT:
            return f'PayPal account {self.email}'
        if self.method_type == self.Type.BANK_ACCOUNT:
            return 'Bank Transfer'
        return f'Unsupported payment method type'

    def type_for_database(self) -> str:
        """Type indication compatible with models.PaymentMethod.METHOD_TYPES

        :return: the two-letter code, or an empty string if unknown.
        """
        try:
            return {
                self.Type.CREDIT_CARD: 'cc',
                self.Type.PAYPAL_ACCOUNT: 'pa',
                self.Type.BANK_ACCOUNT: 'ba',
            }[self.method_type]
        except KeyError:
            return ''


class Registry:
    """Contains an instance for each AbstractPaymentGateway instance.

    This is the primary means of obtaining an interface with a payment provider.
    """
    _log = log.getChild('Registry')
    _instances: typing.Dict[str, MaybeConfiguredGateway] = {}  # initialized lazily
    """Mapping from payment gateway name to instance for that gateway."""

    @classmethod
    def gateway_names(cls) -> typing.Iterable[str]:
        """Return names of the registered gateways."""
        if not cls._instances:
            cls._instantiate_gateways()
        return cls._instances.keys()

    @classmethod
    def instance_for(cls, gateway_name: str) -> 'AbstractPaymentGateway':
        if not cls._instances:
            cls._instantiate_gateways()

        try:
            gw_instance = cls._instances[gateway_name]
        except KeyError:
            raise looper.exceptions.GatewayNotImplemented(
                f'No such Gateway provider {gateway_name!r}')

        if gw_instance is GatewayNotConfigured:
            raise looper.exceptions.GatewayConfigurationMissing(
                f'Gateway provider {gateway_name!r} not configured')

        assert isinstance(gw_instance, AbstractPaymentGateway)
        return gw_instance

    @classmethod
    def _instantiate_gateways(cls) -> None:
        """Create one instance per AbstractPaymentGateway subclass."""
        cls._instances.clear()

        for gw_class in AbstractPaymentGateway.__subclasses__():
            assert issubclass(gw_class, AbstractPaymentGateway)
            assert gw_class is not AbstractPaymentGateway
            try:
                # Ignore the type here, because mypy thinks gw_class is an AbstractPaymentGateway
                # and complains we can't instantiate an abstract class here.
                gw_instance = gw_class()  # type: ignore
            except looper.exceptions.GatewayConfigurationMissing:
                cls._log.error('Gateway provider %r not configured', gw_class.gateway_name)
                # Ignore the type here because we cannot declare the type at the assignment
                # in the try-block.
                gw_instance = GatewayNotConfigured  # type: ignore
            cls._instances[gw_class.gateway_name] = gw_instance


class AbstractPaymentGateway(metaclass=abc.ABCMeta):
    gateway_name: str = ''  # set in each subclass.
    supported_collection_methods: typing.Set[str] = set()
    supports_refunds = True
    supports_transactions = True

    _log = log.getChild('AbstractPaymentGateway')

    def __init__(self) -> None:
        gateway_settings = settings.GATEWAYS.get(self.gateway_name)
        if gateway_settings is None:
            raise looper.exceptions.GatewayConfigurationMissing(
                f'Missing settings for Gateway {self.gateway_name!r}')

        self.settings = gateway_settings

    def __init_subclass__(cls) -> None:
        assert cls.gateway_name, 'subclasses must set gateway_name'
        assert cls.supported_collection_methods, 'subclasses must set supported_collection_methods'
        super().__init_subclass__()
        cls._log = log.getChild(cls.__name__)

    @abc.abstractmethod
    def generate_client_token(self, for_currency: str,
                              gateway_customer_id: typing.Optional[str] = None) -> str:
        """Return a token to be used with the drop-in UI of a gateway."""
        pass

    @abc.abstractmethod
    def customer_create(self) -> str:
        """Create a customer record at the payment provider.

        :return: The customer ID at the payment provider.
        :raise: GatewayError if the user cannot be created.
        """
        pass

    @abc.abstractmethod
    def payment_method_create(self,
                              payment_method_nonce: str,
                              gateway_customer_id: str) -> PaymentMethodInfo:
        """Store a payment method for the specified customer.

        :return: The PaymentMethodInfo, which contains the payment method token.
        :raise: GatewayError if the payment method cannot be created.
        """
        pass

    @abc.abstractmethod
    def payment_method_delete(self, payment_method_token: str) -> None:
        """Delete a payment method for the specified customer."""
        pass

    @abc.abstractmethod
    def find_customer_payment_method(self, payment_method_token: str) -> PaymentMethodInfo:
        """Lookup a payment method and return it as a dictionary."""
        pass

    @abc.abstractmethod
    def transact_sale(self, payment_method_token: str, amount: Money) -> str:
        """Perform a sale transaction.

        :return: The transaction ID of the payment gateway.
        :raise looper.exceptions.CurrencyNotSupported: when the amount has a
            currency for which there is no configured Merchant Account ID.
        :raise looper.exceptions.GatewayError: when there was an error
            performing the transaction at the payment gateway.
        """
        pass

    @abc.abstractmethod
    def find_transaction(self, transaction_id) -> dict:
        """Lookup a transaction and return it as a dictionary."""
        pass

    @abc.abstractmethod
    def refund(self, gateway_transaction_id: str, amount: Money):
        """Refund a transaction.

        :param: gateway_transaction_id: the transaction ID on the gateway.
        :param amount: The amount to refund, must not be more than the original
            transaction amount.

        Note that the amount to refund is non-optional, contrary to, for
        example, the Braintree API. This is to make the amount explicit.
        In the unlikely case that Braintree has a different amount stored
        for the transaction than we have, we should be refunding the amount
        we expect to be refunding. If this is not possible, errors should be
        explicit.
        """
        pass


class MockableGateway(AbstractPaymentGateway):
    """Payment gateway that expects to be mocked.

    Every function will raise an exception. This allows functions to be mocked,
    and unexpected calls to non-mocked functions to be detected.
    """
    gateway_name: str = 'mock'
    supported_collection_methods = {'automatic'}
    _log = log.getChild('MockableGateway')

    # noinspection PyMissingConstructor
    def __init__(self):
        # Dont' call the superclass, it'll expect configuration which we don't need nor want.
        self.settings = {}

    def generate_client_token(self, for_currency: str,
                              gateway_customer_id: typing.Optional[str] = None) -> str:
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.generate_client_token({gateway_customer_id!r})')

    def customer_create(self) -> str:
        raise NotImplementedError(f'Unexpected call to MockableGateway.customer_create()')

    def payment_method_create(self,
                              payment_method_nonce: str,
                              gateway_customer_id: str) -> PaymentMethodInfo:
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.payment_method_create'
            f'({payment_method_nonce!r}, {gateway_customer_id!r})')

    def payment_method_delete(self, payment_method_token: str) -> None:
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.payment_method_delete({payment_method_token!r})')

    def find_customer_payment_method(self, payment_method_token: str) -> PaymentMethodInfo:
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.find_customer_payment_method'
            f'({payment_method_token!r})')

    def transact_sale(self, payment_method_token: str, amount: Money) -> str:
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.transact_sale'
            f'({payment_method_token!r}, {amount!r})')

    def find_transaction(self, transaction_id) -> dict:
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.find_transaction({transaction_id!r})')

    def refund(self, gateway_transaction_id: str, amount: Money):
        raise NotImplementedError(
            f'Unexpected call to MockableGateway.refund({gateway_transaction_id!r}, {amount})')


class BraintreeGateway(AbstractPaymentGateway):
    gateway_name = 'braintree'
    supported_collection_methods = {'automatic'}

    TRANSACTION_SUCCESS_STATUSES = [
        braintree.Transaction.Status.Authorized,
        braintree.Transaction.Status.Authorizing,
        braintree.Transaction.Status.Settled,
        braintree.Transaction.Status.SettlementConfirmed,
        braintree.Transaction.Status.SettlementPending,
        braintree.Transaction.Status.Settling,
        braintree.Transaction.Status.SubmittedForSettlement
    ]

    def __init__(self):
        super().__init__()
        self.braintree = braintree.BraintreeGateway(
            braintree.Configuration(
                environment=self.settings['environment'],
                merchant_id=self.settings['merchant_id'],
                public_key=self.settings['public_key'],
                private_key=self.settings['private_key']
            )
        )

    def _merchant_account_id(self, currency: str) -> str:
        """Return the Merchant Account ID for the given currency.

        Configured in Braintree at Account → Merchant Account Info.
        """
        merchant_account_ids: typing.Mapping[str, str] = self.settings['merchant_account_ids']
        try:
            return merchant_account_ids[currency]
        except KeyError:
            self._log.error('Unsupported currency %r requested', currency)
            supported = set(merchant_account_ids.keys())
            raise looper.exceptions.CurrencyNotSupported(
                f'Currency {currency!r} not supported, only {supported}')

    def generate_client_token(self, for_currency: str,
                              gateway_customer_id: typing.Optional[str] = None) -> str:
        """Generate Braintree client token.

        Returns a string which contains all authorization and configuration
        information our client needs to initialize the Braintree Client SDK to
        communicate with Braintree.

        See https://developers.braintreepayments.com/reference/request/client-token/generate/python
        """

        params = {}
        if gateway_customer_id:
            params['customer_id'] = gateway_customer_id
        if for_currency:
            merchant_account_id = self._merchant_account_id(for_currency)
            params['merchant_account_id'] = merchant_account_id
        return self.braintree.client_token.generate(params)

    def customer_create(self) -> str:
        """Create a customer at BrainTree.

        See https://developers.braintreepayments.com/reference/request/customer/create/python
        """
        self._log.debug('Creating new customer at Braintree')
        result = self.braintree.customer.create({})
        if not result.is_success:
            self._log.debug('Error creating new customer: message=%s  errors=%s',
                            result.message, result.errors)
            raise BraintreeError(result)
        return result.customer.id

    def payment_method_create(self, payment_method_nonce: str, gateway_customer_id: str) \
            -> PaymentMethodInfo:
        """Create a payment method at BrainTree.

        See https://developers.braintreepayments.com/reference/request/payment-method/create/python
        """
        result = self.braintree.payment_method.create({
            'customer_id': gateway_customer_id,
            'payment_method_nonce': payment_method_nonce,
            'options': {
                'make_default': True,
            },
        })
        if not result.is_success:
            self._log.debug('Error creating payment method: message=%s  errors=%s',
                            result.message, result.errors)
            raise BraintreeError(result)

        return PaymentMethodInfo.from_braintree(result.payment_method)

    def payment_method_delete(self, payment_method_token: str) -> None:
        """Delete a payment method at BrainTree.

        See https://developers.braintreepayments.com/reference/request/payment-method/delete/python
        """
        from braintree.exceptions import not_found_error, braintree_error

        try:
            result = self.braintree.payment_method.delete(payment_method_token)
        except not_found_error.NotFoundError:
            # Deleting something that isn't there is fine.
            self._log.debug('Error deleting payment method, token was unknown at Braintree.')
            return
        except braintree_error.BraintreeError as ex:
            self._log.info('Error deleting payment token: %s', ex)
            raise looper.exceptions.GatewayError(message=str(ex))

        if not result.is_success:
            self._log.debug('Error deleting payment method: message=%s  errors=%s',
                            result.message, result.errors)
            raise BraintreeError(result)

    def find_customer_payment_method(self, payment_method_token: str) -> PaymentMethodInfo:
        from braintree.exceptions.not_found_error import NotFoundError
        try:
            payment_method = self.braintree.payment_method.find(payment_method_token)
        except NotFoundError:
            raise looper.exceptions.NotFoundError('Payment gateway not found')

        return PaymentMethodInfo.from_braintree(payment_method)

    def transact_sale(self, payment_method_token: str, amount: Money) -> str:
        """Perform a sale by requesting a monetary amount to be paid.

        See https://developers.braintreepayments.com/reference/request/transaction/sale/python

        :return: The transaction ID of the payment gateway.
        :raise looper.exceptions.CurrencyNotSupported: when the amount has a
            currency for which there is no configured Merchant Account ID.
        :raise looper.exceptions.GatewayError: when there was an error
            performing the transaction at Braintree.
        """
        self._log.debug('Performing transaction of %s', amount)
        ma_id = self._merchant_account_id(amount.currency)

        result = self.braintree.transaction.sale({
            'payment_method_token': payment_method_token,
            'amount': amount.decimals_string,
            'merchant_account_id': ma_id,
            'options': {
                'submit_for_settlement': True,
            },
        })
        if not result.is_success:
            raise BraintreeError(result)

        self._log.info('Transaction %r for amount %s was successful with status=%r',
                       result.transaction.id, amount, result.transaction.status)
        return result.transaction.id

    def find_transaction(self, transaction_id):
        transaction = self.braintree.transaction.find(transaction_id)
        return {'transaction_id': transaction.id}

    def refund(self, gateway_transaction_id: str, amount: Money):
        """Refund a transaction.

        :param: gateway_transaction_id: the transaction ID on the gateway.
        :param amount: The amount to refund, may not be more than the original
            transaction amount.

        See https://developers.braintreepayments.com/reference/request/transaction/refund/python
        """
        from braintree.exceptions import not_found_error, braintree_error

        self._log.debug('Refunding transaction %r for %s', gateway_transaction_id, amount)
        try:
            result = self.braintree.transaction.refund(gateway_transaction_id,
                                                       amount.decimals_string)
        except not_found_error.NotFoundError as ex:
            self._log.debug('Error refunding, transaction %r was unknown at Braintree: %s',
                            gateway_transaction_id, ex)
            raise looper.exceptions.GatewayError(
                message=f'transaction {gateway_transaction_id} was unknown at Braintree')
        except braintree_error.BraintreeError as ex:
            self._log.info('Error refunding transaction %r: %s', gateway_transaction_id, ex)
            raise looper.exceptions.GatewayError(message=str(ex))

        if not result.is_success:
            raise BraintreeError(result)

        self._log.info('Transaction %r refund for amount %s was successful',
                       gateway_transaction_id, amount)


class BankGateway(AbstractPaymentGateway):
    """Payment gateway to support bank payments.

    Requires manual intervention in the admin to mark orders as paid.
    """
    gateway_name: str = 'bank'
    supported_collection_methods = {'manual'}
    supports_refunds = False
    supports_transactions = False
    _log = log.getChild('BankGateway')

    def generate_client_token(self, for_currency: str,
                              gateway_customer_id: typing.Optional[str] = None) -> str:
        return ''

    def customer_create(self) -> str:
        """Always return the same meaningless customer ID."""
        return 'bank'

    def payment_method_create(self,
                              payment_method_nonce: str,
                              gateway_customer_id: str) -> PaymentMethodInfo:
        """Always return the same PaymentMethodInfo"""
        return PaymentMethodInfo('bank', PaymentMethodInfo.Type.BANK_ACCOUNT)

    def payment_method_delete(self, payment_method_token: str) -> None:
        """Does nothing, as the gateway itself (that is, the bank) does nothing."""

    def find_customer_payment_method(self, payment_method_token: str) -> PaymentMethodInfo:
        """Always return the same PaymentMethodInfo"""
        return PaymentMethodInfo('bank', PaymentMethodInfo.Type.BANK_ACCOUNT)

    def transact_sale(self, payment_method_token: str, amount: Money) -> str:
        raise NotImplementedError(
            f'Unexpected call to BankGateway.transact_sale'
            f'({payment_method_token!r}, {amount!r})')

    def find_transaction(self, transaction_id) -> dict:
        raise NotImplementedError(
            f'Unexpected call to BankGateway.find_transaction({transaction_id!r})')

    def refund(self, gateway_transaction_id: str, amount: Money):
        raise NotImplementedError(
            f'Unexpected call to BankGateway.refund({gateway_transaction_id!r}, {amount})')
