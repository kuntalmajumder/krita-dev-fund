from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.documents import urls as wagtaildocs_urls
from wagtail.core import urls as wagtail_urls

from blender_fund_main.views import errors

handler500 = errors.ErrorHandler500.as_view()


urlpatterns = [
    path('oauth/', include('blender_id_oauth_client.urls', namespace='oauth')),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', include('loginas.urls')),
    path('admin/', admin.site.urls),
    path('admin/notes/', include('blender_notes.urls')),

    path('', include('blender_fund_main.urls')),
    path('', include('looper.urls')),

    # Wagtail URLs
    path('cms/', include(wagtailadmin_urls)),
    path('documents/', include(wagtaildocs_urls)),
    path('', include(wagtail_urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + \
    static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
