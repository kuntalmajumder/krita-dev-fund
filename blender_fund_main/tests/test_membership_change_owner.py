from django.contrib.auth.models import User
from django.urls import reverse

from looper.tests import AbstractLooperTestCase
from .. import models


class ChangeUserTest(AbstractLooperTestCase):
    def test_change_user(self):
        new_user = User.objects.create_user(username='henkie', email='henkie@example.com')
        subs = self.create_active_subscription()
        mem: models.Membership = subs.membership

        subs.latest_order().generate_transaction()

        mem.user = new_user
        mem.save(update_fields={'user'})

        # The subscription should be transferred, with orders and transactions.
        new_uid = new_user.id
        mem.refresh_from_db()
        subs.refresh_from_db()
        self.assertEqual(new_uid, mem.user_id)
        self.assertEqual(new_uid, subs.user_id)
        self.assertIsNone(subs.payment_method)
        self.assertEqual([new_uid], [o.user_id for o in subs.order_set.all()])
        self.assertIsNone(subs.order_set.first().payment_method)
        self.assertEqual([new_uid], [t.user_id
                                     for o in subs.order_set.all()
                                     for t in o.transaction_set.all()])

        # After this, getting the receipts (which iterates orders) should still work
        # for both users.
        self.client.force_login(self.user)
        resp = self.client.get(reverse('settings_receipts'))
        self.assertEqual(200, resp.status_code)

        self.client.force_login(new_user)
        resp = self.client.get(reverse('settings_receipts'))
        self.assertEqual(200, resp.status_code)
