import copy
import typing

from django.db import models

OldStateType = typing.Mapping[str, typing.Any]
"""Type declaration for the old state of a model instance.

See RecordModificationMixin.pre_save_record().
"""


class CreatedUpdatedMixin(models.Model):
    """Store creation and update timestamps."""

    class Meta:
        abstract = True

    created_at = models.DateTimeField('date created', auto_now_add=True)
    updated_at = models.DateTimeField('date edited', auto_now=True)


class RecordModifcationMixin(models.Model):
    """Modification tracking for Django models.

    Tracks which fields have changed in the save() function, so that
    the model can send signals upon changes in fields.

    Only acts on fields listed in self.record_modification_fields.
    """

    class Meta:
        abstract = True

    record_modification_fields: typing.Set[str]

    def _check_modification(self, old_instance) -> bool:
        """Returns True iff the membership was modified.

        Only checks fields listed in self.record_modification_fields.
        """

        for field in self.record_modification_fields:
            old_val = getattr(old_instance, field, ...)
            new_val = getattr(self, field, ...)
            if old_val != new_val:
                return True
        return False

    def pre_save_record(self) -> typing.Tuple[bool, OldStateType]:
        """Records the previous state of this object.

        Only records fields listed in self.record_modification_fields.

        :returns: (was changed, old state) tuple.
        """
        if not self.pk:
            return True, {}

        try:
            db_instance = type(self).objects.get(id=self.pk)
        except type(self).DoesNotExist:
            return True, {}

        was_modified = self._check_modification(db_instance)
        old_instance_data = {attr: copy.deepcopy(getattr(db_instance, attr))
                             for attr in self.record_modification_fields}
        return was_modified, old_instance_data


class StateMachineMixin(models.Model):
    """Model with a 'status' field and well-defined status transitions.

    Python code can perform any state transition. Manual changes via the
    admin can be handled by using a custom Admin form that subclasses
    `looper.forms.StateMachineMixin`.
    """

    class Meta:
        abstract = True

    # Map 'current status': {set of allowed new statuses}; define in subclass:
    VALID_STATUS_TRANSITIONS: typing.Mapping[str, typing.Set[str]]

    # Add this field in a subclass, so that it uses the subclass' STATUSES
    # and DEFAULT_STATUS values:
    # status = models.CharField(choices=STATUSES, default=DEFAULT_STATUS, max_length=20)
    status: str

    def may_transition_to(self, new_status: str) -> bool:
        """Validate the potential transition from the current status to 'new_status'."""
        try:
            valid_transitions = self.VALID_STATUS_TRANSITIONS[self.status]
        except KeyError:
            return False
        return new_status in valid_transitions
