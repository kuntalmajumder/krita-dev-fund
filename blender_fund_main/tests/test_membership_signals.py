from django.contrib.auth.models import User
import django.core.mail
from django.dispatch import receiver
from django.test import override_settings

from looper.tests import AbstractBaseTestCase


class MembershipActivationSignalTest(AbstractBaseTestCase):
    fixtures = ['default_site', 'devfund']

    def setUp(self):
        super().setUp()
        self.user = User.objects.create_user('harry', 'harry@blender.org')
        self.full_name = 'Harry de Bøker'
        self.user.customer.full_name = self.full_name

    def test_create_activated(self):
        from ..signals import membership_activated
        from ..models import Membership

        signal_old_status = ''
        signal_new_status = ''
        signal_count = 0

        @receiver(membership_activated)
        def activated(sender: Membership, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_count
            signal_old_status = old_status
            signal_new_status = sender.status
            signal_count += 1

        mem = Membership.objects.create(
            level_id=1,
            status='active',
            display_name='je moeder',
            user=self.user,
        )

        self.assertEqual('', signal_old_status)
        self.assertEqual('active', signal_new_status)
        self.assertEqual(1, signal_count)

        mem.status = 'active'
        mem.save()

        self.assertEqual(1, signal_count,
                         'Re-saving without modification of the status should not trigger signal')

        # An email should have been sent to the owner of the membership.
        self.assertEqual(1, len(django.core.mail.outbox))
        the_mail: django.core.mail.message.EmailMultiAlternatives = django.core.mail.outbox[0]
        self.assertIn(self.full_name, the_mail.body)
        alt0_body, alt0_type = the_mail.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')

    def test_activate_after_creation(self):
        from ..signals import membership_activated, membership_deactivated
        from ..models import Membership

        signal_old_status = ''
        signal_new_status = ''
        signal_count = signal_deact_count = 0

        @receiver(membership_activated)
        def activated(sender: Membership, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_count
            signal_old_status = old_status
            signal_new_status = sender.status
            signal_count += 1

        @receiver(membership_deactivated)
        def deactivated(sender: Membership, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_deact_count
            signal_deact_count += 1

        mem = Membership.objects.create(
            level_id=1,
            status='pending',
            display_name='je moeder',
            user=self.user,
        )

        self.assertEqual('', signal_old_status)
        self.assertEqual('', signal_new_status)
        self.assertEqual(0, signal_count)
        self.assertEqual(0, signal_deact_count,
                         'Creating a non-active Membership should not send the deactivated signal')

        mem.status = 'active'
        mem.save()

        self.assertEqual('pending', signal_old_status)
        self.assertEqual('active', signal_new_status)
        self.assertEqual(1, signal_count)

    def test_deactivate(self):
        from ..signals import membership_deactivated
        from ..models import Membership

        signal_old_status = ''
        signal_new_status = ''
        signal_count = 0

        @receiver(membership_deactivated)
        def deactivated(sender: Membership, old_status: str, **kwargs):
            nonlocal signal_old_status, signal_new_status, signal_count
            signal_old_status = old_status
            signal_new_status = sender.status
            signal_count += 1

        mem = Membership.objects.create(
            level_id=1,
            status='active',
            display_name='je moeder',
            user=self.user,
        )

        self.assertEqual(0, signal_count)

        mem.status = 'inactive'
        mem.save()

        self.assertEqual('active', signal_old_status)
        self.assertEqual('inactive', signal_new_status)
        self.assertEqual(1, signal_count)

    def test_pending_to_inactive(self):
        from ..signals import membership_activated, membership_deactivated
        from ..models import Membership

        signal_count = 0

        @receiver(membership_activated)
        def activated(sender: Membership, **kwargs):
            nonlocal signal_count
            signal_count += 1

        @receiver(membership_deactivated)
        def deactivated(sender: Membership, **kwargs):
            nonlocal signal_count
            signal_count += 1

        mem = Membership.objects.create(
            level_id=1,
            status='pending',
            display_name='je moeder',
            user=self.user,
        )

        self.assertEqual(0, signal_count)

        mem.status = 'inactive'
        mem.save()

        self.assertEqual(0, signal_count)
