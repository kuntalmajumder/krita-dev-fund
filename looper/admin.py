import copy
import typing

import django.db.models
from django.contrib import admin
from django.utils.html import format_html
from django.utils.safestring import SafeText
from django.urls import reverse

from looper.utils import make_absolute_url
from . import forms, models, decorators

USER_SEARCH_FIELDS = ('user__email', 'user__customer__billing_email',
                      'user__customer__full_name', 'user__username')


def interval(instance) -> str:
    """Combines interval_unit and interval_length into one string.

    This helps making the admin's list_display a little bit tidier.
    """
    if instance.interval_length == 1:
        return instance.interval_unit
    return f'{instance.interval_length} {instance.interval_unit}'


@decorators.short_description('ID')
def id_column(instance) -> str:
    pk = instance.pk
    if pk is None:
        return '-'
    if isinstance(pk, int):
        return f'#{pk}'
    return str(pk)


@decorators.short_description('')
def create_subscription_button_link(planvar: models.PlanVariation) -> str:
    assert isinstance(planvar, models.PlanVariation), \
        f'Expected PlanVariation, not {type(planvar)}'
    if planvar.pk is None:
        return ''

    from urllib.parse import urlencode
    button_url = reverse('admin:looper_subscription_add')
    query = urlencode({
        'plan': planvar.plan_id,
        'collection_method': 'manual',
        'currency': planvar.currency,
        'price': planvar.price.decimals_string,
        'interval_unit': planvar.interval_unit,
        'interval_length': planvar.interval_length,
    })

    return format_html(
        '<span class="object-tools"><a class="historylink" href="{}">Create Subscription</a></span>',
        f'{button_url}?{query}')


class PlanVariationInline(admin.TabularInline):
    model = models.PlanVariation
    min_num = 1
    extra = 0
    fields = [id_column, 'currency',
              'price', 'interval_unit', 'interval_length', 'is_default_for_currency',
              'collection_method', 'is_active', create_subscription_button_link]
    readonly_fields = [id_column, create_subscription_button_link]


@admin.register(models.Gateway)
class GatewayAdmin(admin.ModelAdmin):
    list_display = ['name', 'is_default', 'frontend_name']
    list_display_links = list_display


admin.site.register(models.Product)

LinkFunc = typing.Callable[[django.db.models.Model], str]


def create_admin_fk_link(field_name: str, short_description: str, view_name: str) -> LinkFunc:
    """Construct a function that constructs a link to the admin:xxxx_change form.

    :param field_name: The object is taken from model_instance.{field_name}
    :param short_description: The label for this link as shown in the admin.
    :param view_name: The admin view to link to. Must take one parameter `object_id`.

    For example, to create a link to a customer (instead of a drop-down to edit it),
    use:

    `customer_link = create_admin_fk_link('customer', 'Customer', 'admin:looper_customer_change')`
    """

    def create_link(model_instance) -> str:
        referenced = getattr(model_instance, field_name)
        if referenced is None:
            return '-'
        assert isinstance(referenced, django.db.models.Model), \
            f'Expected Model, not {type(referenced)}'
        admin_link = reverse(view_name,
                             kwargs={'object_id': referenced.pk})
        return format_html('<a href="{}">{}</a>', admin_link, str(referenced))

    # Callable[[Model], str] doesn't have those Django-specific attributes,
    # and I don't feel like speccing that all out. ~~Sybren
    create_link.admin_order_field = field_name  # type: ignore
    create_link.short_description = short_description  # type: ignore
    return create_link


def customer_link(model_instance: models.Customer) -> str:
    customer = getattr(model_instance, 'customer')
    if customer is None:
        return '-'
    user = customer.user
    admin_link = reverse('admin:auth_user_change', kwargs={'object_id': user.pk})
    return format_html('<a href="{}">{}</a>', admin_link, str(customer))


customer_link.admin_order_field = 'customer'  # type: ignore

# TODO(Sybren): this is a total hack, as the membership is only a single product, and
# there can be subscriptions without memberships. Furthermore, this creates a dependency
# cycle between Blender Fund Main and Looper. For now it's a quick way to get the link, though.
membership_link = create_admin_fk_link('membership', 'Membership',
                                       'admin:blender_fund_main_membership_change')
subscription_link = create_admin_fk_link('subscription', 'subscription',
                                         'admin:looper_subscription_change')
order_link = create_admin_fk_link('order', 'order', 'admin:looper_order_change')
plan_link = create_admin_fk_link('plan', 'plan', 'admin:looper_plan_change')
product_link = create_admin_fk_link('product', 'product', 'admin:looper_product_change')
user_link = create_admin_fk_link('user', 'user', 'admin:auth_user_change')


@admin.register(models.Plan)
class PlanAdmin(admin.ModelAdmin):
    list_display = [id_column, 'name', 'is_active', product_link]
    list_display_links = [id_column, 'name', 'is_active']
    inlines = [
        PlanVariationInline,
    ]


def copy_to_clipboard_link(view_name: str, kwargs: dict, link_text: str, title: str) -> SafeText:
    """Construct a link that copies a link to the clipboard when clicked.

    This is for things like payment links, e.g. links that should be sent to
    a customer (instead of followed by the admin).
    """
    link = reverse(view_name, kwargs=kwargs)
    link = make_absolute_url(link)

    return format_html(
        '<a href="{}" title="{}" data-clipboard-text="{}">{}</a><br>'
        '<span class="help">{}</span>',
        link, title, link, link_text, 'Click link to copy')


@decorators.short_description('Payment Link')
def payment_link(model_instance: typing.Optional[models.Order]) -> str:
    """Show the payment link, can be sent to users."""
    if not model_instance or not model_instance.pk:
        return ''

    return copy_to_clipboard_link('looper:checkout_existing_order',
                                  kwargs={'order_id': model_instance.pk},
                                  link_text='Payment Link',
                                  title='Send this to customers')


class OrderInline(admin.TabularInline):
    model = models.Order
    min_num = 0
    extra = 0
    show_change_link = True
    fields = ['status', 'created_at', 'updated_at', 'price', 'payment_method', 'collection_method',
              'collection_attempts', payment_link]
    readonly_fields = fields
    can_delete = False


FieldsetType = typing.Sequence[typing.Tuple[typing.Optional[str], typing.Dict[str, typing.Any]]]


class EditableWhenNewMixin:
    # These should be set on the subclass using this mix-in.
    readonly_fields: typing.Sequence[typing.Union[str, typing.Callable]]
    fieldsets: FieldsetType

    # Those callable fields will become editable when adding a new instance.
    # Assumes that these callables have a 'admin_order_field' property that
    # contains the field they map to.
    editable_when_new: typing.Set[typing.Callable] = set()

    def get_readonly_fields(self, request, obj=None):
        if obj:  # Editing an existing object
            return self.readonly_fields

        # When adding a new subscription, remove the the 'xxx_link' callables
        # from the readonly_fields list. This optimises for the more common
        # case of subscriptions being created by the checkout process, rather
        # than manually via the admin.
        return [field for field in self.readonly_fields
                if not hasattr(field, '__call__')]

    def get_fieldsets(self, request, obj=None) -> FieldsetType:
        if obj:  # Editing an existing object.
            return self.fieldsets

        fieldsets = copy.deepcopy(self.fieldsets)

        for fieldset in fieldsets:
            label, config = fieldset
            if 'fields' not in config:
                continue

            fields = []
            # Replace callables with the field name they map to (using
            # the_callable.admin_order_field), or remove callables if
            # they are not in self.editable_when_new.
            for field in config['fields']:
                if not hasattr(field, '__call__'):
                    fields.append(field)
                    continue
                if field not in self.editable_when_new:
                    continue
                assert hasattr(field, 'admin_order_field'), \
                    f"callable {field} should have an attribute 'admin_order_field'"
                fields.append(field.admin_order_field)
            config['fields'] = fields
        return fieldsets


@admin.register(models.Subscription)
class SubscriptionAdmin(EditableWhenNewMixin, admin.ModelAdmin):
    list_display = ['id', 'plan', 'status',
                    'started_at', 'cancelled_at',
                    interval, 'intervals_elapsed',
                    user_link, 'payment_method', 'collection_method']
    list_display_links = ['id', 'plan', 'status', 'started_at', 'cancelled_at']
    list_filter = ['plan', 'status',
                   'created_at', 'started_at', 'cancelled_at',
                   'collection_method']
    search_fields = ['id', *USER_SEARCH_FIELDS]
    date_hierarchy = 'created_at'

    form = forms.SubscriptionAdminForm
    raw_id_fields = ['user']

    readonly_fields = [user_link, plan_link, membership_link,
                       'created_at', 'updated_at', 'intervals_elapsed', 'last_notification']
    editable_when_new = {user_link, plan_link}
    fieldsets = [
        (None, {
            'fields': [user_link, plan_link, membership_link, 'status',
                       'interval_unit', 'interval_length', 'intervals_elapsed'],
        }),
        ('Money', {
            'fields': ['payment_method', 'collection_method',
                       'currency', 'price',
                       # 'tax', 'tax_type', 'tax_region',
                       ],
        }),
        ('Dates', {
            'fields': ['created_at', 'updated_at',
                       'started_at', 'cancelled_at',
                       'current_interval_started_at',
                       'next_payment', 'last_notification'],
        }),
    ]

    inlines = [
        OrderInline,
    ]


def refund_button_link(trans: models.Transaction) -> str:
    assert isinstance(trans, models.Transaction), \
        f'Expected Model, not {type(trans)}'
    if trans.pk is None:
        return ''
    refund_link = reverse('admin:looper_transaction_change',
                          kwargs={'object_id': trans.pk})
    return format_html(
        '<span class="object-tools"><a class="historylink" href="{}">View to refund</a></span>',
        refund_link)


class TransactionsInline(admin.TabularInline):
    model = models.Transaction
    min_num = 0
    extra = 0
    show_change_link = True
    fields = ['status', 'created_at', 'updated_at', 'failure_message',
              'amount', 'amount_refunded', 'refunded_at', refund_button_link]
    readonly_fields = fields
    can_delete = False


def mark_as_paid(modeladmin, request, queryset):
    """Mark all selected orders as 'paid'."""
    for order in queryset:
        order.status = 'paid'
        order.save()


# TODO(Sybren): this is a total hack, as the receipt PDF view is outside of Looper.
@decorators.short_description('Receipt')
def order_receipt_link(order: models.Order) -> str:
    if order.status != 'paid':
        return format_html('<span title="{}">-</span>', 'Only available for paid orders')

    pdf_url = reverse('settings_receipt_pdf', kwargs={'order_id': order.id})
    return format_html('<a href="{}" target="_blank">PDF</a>', pdf_url)


@admin.register(models.Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'status', 'created_at', 'paid_at', 'price',
                    user_link, subscription_link,
                    'payment_method', 'collection_method', order_receipt_link]
    list_display_links = ['id', 'status', 'created_at', 'paid_at']
    list_filter = ['status', 'created_at', 'collection_method', 'payment_method__gateway']
    search_fields = ['id', 'email', *USER_SEARCH_FIELDS]

    actions = [mark_as_paid]

    form = forms.OrderAdminForm
    raw_id_fields = ['user']
    readonly_fields = [user_link, subscription_link, payment_link, 'created_at', 'updated_at',
                       'collection_attempts', 'total_refunded']
    fieldsets = [
        (None, {
            'fields': [user_link, subscription_link, payment_link, 'status', 'name'],
        }),
        ('Money', {
            'fields': ['payment_method', 'collection_method', 'collection_attempts',
                       'currency', 'price', 'total_refunded',
                       # 'tax', 'tax_type', 'tax_region',
                       ],
        }),
        ('Dates', {
            'fields': ['created_at', 'updated_at', 'paid_at', 'retry_after'],
            'classes': ('collapse',),
        }),
        ('Addresses', {
            'fields': ['email', 'billing_address'],
            'classes': ('collapse',),
        }),
    ]
    inlines = [
        TransactionsInline,
    ]

    def total_refunded(self, order: models.Order) -> str:
        refunded = order.total_refunded()
        if not refunded:
            return '-'
        return refunded.with_currency_symbol_nonocents()


@admin.register(models.Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ('id', 'status', 'created_at', 'updated_at',
                    'amount', 'amount_refunded', 'refunded_at')
    list_display_links = ('id', 'status', 'created_at', 'updated_at')
    list_filter = ('status', 'created_at')
    search_fields = ('id', 'transaction_id', *USER_SEARCH_FIELDS)

    form = forms.AdminTransactionRefundForm
    fieldsets = [
        (None, {
            'fields': [user_link, order_link,
                       'status', 'failure_message', 'paid', 'transaction_id'],
        }),
        ('Dates', {
            'fields': ['created_at', 'updated_at'],
        }),
        ('Money', {
            'fields': ['payment_method', 'currency', 'amount',
                       'refunded_at', 'amount_refunded', 'refund_amount'],
        }),
    ]

    # Make everything read-only except the amount to refund.
    readonly_fields = (user_link, order_link, 'status', 'failure_message', 'paid',
                       'transaction_id', 'created_at', 'updated_at', 'payment_method',
                       'currency', 'amount', 'refunded_at',
                       'amount_refunded')


class CustomerInline(admin.StackedInline):
    model = models.Customer
    can_delete = False
    # It's a one-on-one relationship, so users have only one 'customer'.
    verbose_name_plural = 'customer'


class AddressInline(admin.StackedInline):
    model = models.Address
    can_delete = False
    extra = 0
    max_num = 1
    # Right now users only have one address, which is always a billing address.
    verbose_name = 'Billing Address'
    verbose_name_plural = 'Billing Address'
    exclude = ['category']


class PaymentMethodInline(admin.TabularInline):
    model = models.PaymentMethod
    can_delete = False
    extra = 0
    max_num = 0

    fields = ('method_type', 'is_deleted', 'token', 'recognisable_name')
    readonly_fields = ('method_type', 'recognisable_name', 'token')

    def get_queryset(self, request):
        return super().get_queryset(request).exclude(is_deleted=True)
