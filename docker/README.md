# Blender Development Fund deployment

Deployment is done via Docker, and uses two images. The `blenderinstitute/blender-fund-base`
image contains all the dependencies, including the virtual environment. The
`blenderinstitute/blender-fund` image builds up from this, and adds the project sources.

There are two ways of deployment, 'quick' and 'full'. Either approach will use the current
`origin/production` branch to create the docker images.

## Full Deployment

Use this method when you've never built the docker images, or when you have a reason to
rebuild the base image (security updates, dependency changes).

## Quick Deployment

Use this method to quickly deploy a new version of just the project files. If you don't know
if the dependencies changed, just use the full build.

## Deployment Steps

Run these scripts from the `docker` directory.

For a **quick** deployment:

- `./deploy.sh sintel.blender.org`

For a **full** deployment:

- `./prep_docker_img.sh`
- `./build_docker_img.sh full`
- `./2server.sh sintel.blender.org`

Install this cron job in `/etc/cron.d/blender-fund` on the host:

    PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
    MAILTO=your-address@example.com

    30   * * * * root docker exec --user blender-fund blender-fund /manage.sh clearsessions
    0    * * * * root docker exec --user blender-fund blender-fund /manage.sh clock_tick
    */5  * * * * root docker exec --user blender-fund blender-fund /manage.sh flush_badger_queue --flush -v0


## TLS Certificates

TLS certificates for HTTPS traffic are managed via the Traefik docker image.
