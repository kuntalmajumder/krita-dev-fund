from django.urls import reverse_lazy

from looper.middleware import PREFERRED_CURRENCY_SESSION_KEY
from . import AbstractBaseTestCase

# NL server resolver.xs4all.nl.
EURO_IPV6 = '2001:888:0:9::99'
EURO_IPV4 = '194.109.6.66'

# USA server e.root-servers.net (NASA-managed).
USA_IPV6 = '2001:500:a8::e'
USA_IPV4 = '192.203.230.10'

# Singapore government website gov.sg.
SINGAPORE_IPV4 = '203.208.234.3'
SINGAPORE_IPV6 = '2404:5800:105:153::2'


class PreferredCurrencyTestCase(AbstractBaseTestCase):
    url = reverse_lazy('looper:preferred_currency')

    def test_get_euro_country_ipv6(self):
        resp = self.client.get(self.url, REMOTE_ADDR=EURO_IPV6)
        self.assertEqual({'preferred_currency': 'EUR'}, resp.json())

    def test_get_euro_country_ipv4(self):
        resp = self.client.get(self.url, REMOTE_ADDR=EURO_IPV4)
        self.assertEqual({'preferred_currency': 'EUR'}, resp.json())

    def test_get_euro_country_ipv6_behind_proxy(self):
        resp = self.client.get(self.url, REMOTE_ADDR='::1', HTTP_X_FORWARDED_FOR=EURO_IPV6)
        self.assertEqual({'preferred_currency': 'EUR'}, resp.json())

    def test_get_euro_country_ipv6_chain(self):
        # Such chains are invalid, but can happen when people send invalid HTTP headers.
        resp = self.client.get(self.url, REMOTE_ADDR=f'{EURO_IPV6},::1')
        self.assertEqual({'preferred_currency': 'USD'}, resp.json())

    def test_get_usa_ipv6(self):
        # NASA-managed DNS root server e.root-servers.net.
        resp = self.client.get(self.url, REMOTE_ADDR=USA_IPV6)
        self.assertEqual({'preferred_currency': 'USD'}, resp.json())

    def test_get_usa_ipv4(self):
        # NASA-managed DNS root server e.root-servers.net.
        resp = self.client.get(self.url, REMOTE_ADDR=USA_IPV4)
        self.assertEqual({'preferred_currency': 'USD'}, resp.json())

    def test_get_other_ipv6(self):
        # NASA-managed DNS root server e.root-servers.net.
        resp = self.client.get(self.url, REMOTE_ADDR=SINGAPORE_IPV6)
        self.assertEqual({'preferred_currency': 'USD'}, resp.json())

    def test_get_other_ipv4(self):
        # NASA-managed DNS root server e.root-servers.net.
        resp = self.client.get(self.url, REMOTE_ADDR=SINGAPORE_IPV4)
        self.assertEqual({'preferred_currency': 'USD'}, resp.json())

    def test_set_currency(self):
        resp = self.client.post(self.url, data={'preferred_currency': 'EUR'})
        self.assertEqual(200, resp.status_code)
        self.assertEqual({'preferred_currency': 'EUR'}, resp.json())

        self.assertEqual('EUR', self.client.session[PREFERRED_CURRENCY_SESSION_KEY])

        resp = self.client.get(self.url, REMOTE_ADDR=SINGAPORE_IPV6)
        self.assertEqual({'preferred_currency': 'EUR'}, resp.json())

    def test_set_unsupported_currency(self):
        resp = self.client.post(self.url, data={'preferred_currency': 'SGD'})
        self.assertEqual(400, resp.status_code)

        resp = self.client.get(self.url, REMOTE_ADDR=SINGAPORE_IPV6)
        self.assertEqual({'preferred_currency': 'USD'}, resp.json())

    def test_set_missing_currency(self):
        resp = self.client.post(self.url, data={})
        self.assertEqual(400, resp.status_code)

        resp = self.client.get(self.url, REMOTE_ADDR=SINGAPORE_IPV6)
        self.assertEqual({'preferred_currency': 'USD'}, resp.json())
