def short_description(description):
    """Set wrapped.short_description to 'description'.

    This makes it possible to stick to PEP8 (two newlines before/after
    function) and still keep the short description next to the function
    itself.
    """

    def decorator(wrapped):
        wrapped.short_description = description
        return wrapped

    return decorator
