# File to be included for each vhost to centrally manage common security settings
# WARNING: Do NOT change this file to .conf, or it will be parsed by apache as
# a config instead of included!

# Some other security related headers
# NOTE: Avoiding use of includeSubdomains in case "blender.org" alias causes it
# to match every subdomain of blender.org, at least until we are ready -- Dan
Header always set Strict-Transport-Security "max-age=63072000; preload"
Header always set X-Frame-Options "SAMEORIGIN"
Header always set X-Xss-Protection "1; mode=block"
Header always set X-Content-Type-Options "nosniff"
#Header always set Content-Security-Policy "default-src https:; script-src 'self'; style-src 'unsafe-inline'"
