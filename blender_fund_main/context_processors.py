"""
Makes 'settings' available as template variable.
"""

import typing

import django.conf
from django.http import HttpRequest
from django.urls import ResolverMatch


def settings(request):
    return {'settings': django.conf.settings}


def page_id(request: HttpRequest) -> dict:
    """Inject a 'page id' that's obtained from the view function or view name.

    When using class-based views, set the `page_id` attribute on the class.
    When using function-based views, set the `page_id` attribute on the function.

    When the current view cannot be determined, {'page_id': ''} is returned.
    """
    resolver_match: typing.Optional[ResolverMatch] = request.resolver_match
    if resolver_match is None:
        return {'page_id': ''}

    view_func = resolver_match.func

    try:
        # For class-based views, view_func is what View.as_view() returned.
        view_page_id = view_func.view_class.page_id
    except AttributeError:
        try:
            # For function-based views, view_func is the function itself.
            view_page_id = view_func.page_id
        except AttributeError:
            # If there is no page_id, we reuse the view name.
            view_page_id = resolver_match.view_name.replace(':', '-')

    return {'page_id': view_page_id}
