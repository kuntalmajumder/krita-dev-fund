Dear {{ customer.full_name|default:user.email }},

Your Blender Fund membership was activated! Thanks for your support.

You can always go to {{ link }} to view and update your membership.

Kind regards,

Blender Foundation
