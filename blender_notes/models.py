from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.db import models
from django.db.models import query


class NoteManager(models.Manager):
    def get_by_natural_key(self,
                           app_label: str,
                           model_class_name: str,
                           object_id: int) -> query.QuerySet:
        ctype = ContentType.objects.get_by_natural_key(app_label, model_class_name)
        return self.filter(content_type__pk=ctype.pk, object_id=object_id)

    def get_by_content_type_id(self, content_type_id: int, object_id: int):
        return self.filter(content_type__pk=content_type_id, object_id=object_id)


class Note(models.Model):
    """CRM-like note that can be added to any Django model instance."""

    class Meta:
        ordering = ('created',)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(db_index=True)
    object = GenericForeignKey('content_type', 'object_id')

    creator = models.ForeignKey(User,
                                null=True,
                                blank=True,
                                related_name='created_notes',
                                on_delete=models.PROTECT,
                                limit_choices_to={'is_staff': True})
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    note = models.TextField(blank=False)

    objects = NoteManager()

    def __str__(self):
        return f'Note on {self.content_type} {self.object_id}'
