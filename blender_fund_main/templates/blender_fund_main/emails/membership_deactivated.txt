Dear {{ customer.full_name|default:user.email }},

Your Blender Fund membership was deactivated.

You can always go to {{ link }} and reactivate your membership.

Kind regards,

Blender Foundation
