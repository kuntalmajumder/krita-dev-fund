# Generated by Django 2.1.1 on 2018-09-25 16:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('looper', '0013_price_default_zero'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='company',
            field=models.CharField(blank=True, default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='address',
            name='extended_address',
            field=models.CharField(blank=True, default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='address',
            name='locality',
            field=models.CharField(help_text='City, village, etc.', max_length=255),
        ),
        migrations.AlterField(
            model_name='address',
            name='postal_code',
            field=models.CharField(blank=True, default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='address',
            name='region',
            field=models.CharField(blank=True, default='', max_length=255),
        ),
    ]
